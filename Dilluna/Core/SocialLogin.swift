//
//  SocialLoginManager.swift
//  HappyUAE
//
//  Created by AhmeDroid on 10/17/16.
//  Copyright © 2016 Imagine Technologies. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import GoogleSignIn

class SocialLogin: NSObject {
    
    private static var facebook = LoginManager()
    class func loginUsingFacebook(from: UIViewController, failure:@escaping (Error?)->(Void), success:@escaping (AccessToken, SocialLoginInfo)-> (Void)) -> Void {
        
        facebook.logIn(readPermissions: [.publicProfile, .email], viewController: from) { (result) in
            
            switch result {
                
            case .cancelled:
                print("Facebook logging-in cancelled ..")
                failure(nil)
            case .failed(let error):
                print("Error logging-in Facebook: \(error.localizedDescription)")
                failure(error)
            case .success(_, _, let token):
                
                print("Facebook logging-in token received successfully: \(token)")
                
                let request = GraphRequest(graphPath: "/\(token.userId!)?fields=email,name,picture.type(large)")
                let connection = GraphRequestConnection()
                connection.add(request, batchParameters: nil, completion: { (_, result) in
                    
                    switch result {
                    case .failed(let error):
                        print("Error request Facebook user info: \(error.localizedDescription)")
                        failure(error)
                    case .success(let response):
                        
                        if let info = response.dictionaryValue as [String: AnyObject]? {
                            
                            if let id = info["id"] as? String,
                                let email = info["email"] as? String,
                                let name = info["name"] as? String {
                                
                                let socialInfo = SocialLoginInfo(name: name, id: id, email: email, network: .Facebook)
                                
                                if let picture = info["picture"] as? [String: AnyObject],
                                    let imageData = picture["data"] as? [String: AnyObject],
                                    let imageUrl = imageData["url"] as? String {
                                    
                                    socialInfo.imageUrl = imageUrl
                                }
                                
                                success(token, socialInfo)
                            } else {
                                
                                print("Error getting correct Facebook user info.")
                                failure(nil)
                            }
                            
                        } else {
                            
                            print("Error - no response for Facebook user info request.")
                            failure(nil)
                        }
                    }
                })
                
                connection.start()
            }
        }
    }
    
    
    private static var _googleDelegate = AppGoogleSignInDelegate()
    class func loginUsingGoogle(from: GIDSignInUIDelegate, failure:@escaping (Error?)->(Void), success:@escaping (GIDGoogleUser, SocialLoginInfo)-> (Void)) -> Void {
        
        GIDSignIn.sharedInstance().delegate = _googleDelegate
        GIDSignIn.sharedInstance().uiDelegate = from
        
        _googleDelegate.signInHandler = { (signIn, user, error) in
            
            if error != nil {
                
                print("Error logging-in Google: \(error!.localizedDescription)")
                failure(error)
                
            } else if let userData = user {
                
                let socialInfo = SocialLoginInfo(name: userData.profile.name,
                                                 id: userData.userID,
                                                 email: userData.profile.email,
                                                 network: .Google)
                
                if userData.profile.hasImage {
                    socialInfo.imageUrl = userData.profile.imageURL(withDimension: 500).absoluteString
                }
                
                success(user!, socialInfo)
            } else {
                
                print("Error - no response for Google login request.")
                failure(nil)
            }
        }
        
        _googleDelegate.disconnectHandler = { (signIn, user, error) in
            
            print("Google User disconnected: \( String(describing: user?.userID))")
        }
        
        GIDSignIn.sharedInstance().signIn()
    }
    
    class func logout(_ network:SocialNetwork ) -> Void {
        switch network {
        case .Google:
            GIDSignIn.sharedInstance().signOut()
        case .Facebook:
            facebook.logOut()
        }
    }
}

class AppGoogleSignInDelegate: NSObject, GIDSignInDelegate {
    
    var signInHandler:(GIDSignIn?, GIDGoogleUser?, Error?) -> (Void) = {_ in}
    var disconnectHandler:(GIDSignIn?, GIDGoogleUser?, Error?) -> (Void) = {_ in}
    
    // MARK - Google Sign-in Delegate Methods
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        signInHandler(signIn, user, error)
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        disconnectHandler(signIn, user, error)
    }
}

class SocialLoginInfo {

    var name:String
    var socialId:String
    var socialNetwork:SocialNetwork
    var email:String
    var imageUrl:String?
    
    init(name:String, id:String, email:String, network:SocialNetwork) {
        self.name = name
        self.socialId = id
        self.email = email
        self.socialNetwork = network
    }
}

enum SocialNetwork:Int {
    case Facebook = 1
    case Google = 2
}

