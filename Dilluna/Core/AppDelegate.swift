//
//  AppDelegate.swift
//  OffersMatch
//
//  Created by Ali Hajjaj on 3/14/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import TrueTime
import Firebase
import UserNotifications
import Alamofire
import FacebookCore
import FacebookLogin
import GoogleSignIn


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var isLaunchedWithNotfication:Bool = false
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if AppUserInfo.current.isSignedIn && AppUserInfo.current.isConfirmed == false {
            AppUserInfo.current.logout()
        }
        
        if AppUserInfo.current.searchRadius == 0.0 {
            AppUserInfo.current.searchRadius = 2000
        }
        
        TrueTimeClient.sharedInstance.start()
        
        FirebaseApp.configure()
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: nil)
        
        GMSServices.provideAPIKey(PlacesWebAPI.apiKey)
        GMSPlacesClient.provideAPIKey(PlacesWebAPI.apiKey)
        
        LocationService.shared.requestAuthorization()
        LocationService.shared.updateLocation()
        
        Locale.setupInitialLanguage()
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        Messaging.messaging().delegate = self
        
        // Social Media Setup
        GIDSignIn.sharedInstance().clientID = GoogleKeys.clientID
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        if let _ = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable: Any]  {
            self.isLaunchedWithNotfication = true
        }
        
        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
        
        if let userId = AppUserInfo.current.userId {
            AppWebApi.submitToken(fcmToken, forMember: userId)
        } else {
            AppWebApi.submitVisitor(withToken: fcmToken)
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.sound, .alert])
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("Fetch for remote !!")
        self.investigateNotification(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print("NOTIFICATION RECEIVED")
        self.investigateNotification(userInfo)
    }
    
    func investigateNotification(_ userInfo:[AnyHashable : Any]) -> Void {
        
        self.investigateUserInfo(userInfo, appLaunch: self.isLaunchedWithNotfication)
        self.isLaunchedWithNotfication = false
    }
    
    func investigateUserInfo(_ userInfo:[AnyHashable : Any], appLaunch:Bool = false) -> Void {
        
        print(userInfo)
        
        var adCategory:String?
        if let _adCat = userInfo["ads_category"] as? Int {
            adCategory = String(_adCat)
        } else if let _adCat = userInfo["ads_category"] as? String {
            adCategory = _adCat
        }
        
        if let cat = adCategory {
            
            NotificationCenter.default.post(name: NotificationNames.AppLaunchUsingAdCategoryNotification,
                                            object: nil,
                                            userInfo: ["ads_category": cat, "app_launch": appLaunch])
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if url.scheme!.contains("fb") {
            
            return SDKApplicationDelegate.shared.application(app, open: url, options: options)
            
        } else if url.scheme!.contains("google")  {
            
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        } else {
            
            print("Will open:")
            print(url)
            return true
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        NotificationCenter.default.post(name: NotificationNames.AppDidBecomeActive, object: nil)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
        if AppUserInfo.current.isSignedIn && AppUserInfo.current.isConfirmed == false {
            AppUserInfo.current.logout()
        }
    }
}
