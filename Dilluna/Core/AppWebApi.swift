//
//  WebApi.swift
//  OffersMatch
//
//  Created by Ali Hajjaj on 3/15/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD
import SwiftyJSON
import CoreLocation
import GooglePlaces

class AppWebApi {
    
    //private static let base = "http://localhost:5000"
    private static let base = "http://dilluna.com.10-0-0-4.mint.imagine.com.jo"
    
    typealias ApiCallCompletionHandler = (JSON) -> String?
    typealias FailureHandler = (String) -> Void
    
    private static func callApi(loadingComment:String? = nil,
                                endpoint:String,
                                method:HTTPMethod = .post,
                                params:[String:Any],
                                encoding:ParameterEncoding = URLEncoding.default,
                                completion:@escaping ApiCallCompletionHandler,
                                failure:FailureHandler? = nil) -> Void {
        
        SVProgressHUD.setDefaultMaskType(.gradient)
        
        if let comment = loadingComment {
            SVProgressHUD.show(withStatus: comment)
        } else {
            SVProgressHUD.show()
        }
        
        let request:DataRequest
        if method == .get {
            request = Alamofire.request("\(base)/\(endpoint)", method: .get, parameters: params)
        } else {
            request = Alamofire.request("\(base)/\(endpoint)", method: .post, parameters: params, encoding: encoding)
        }
        
        request.responseSwiftyJSON { (response) in
            
            SVProgressHUD.dismiss()
            
            var errorStr:String?
            if let error = response.error {
                
                errorStr = error.localizedDescription
                
            } else if let json = response.value {
                
                errorStr = completion(json)
                
            } else {
                
                errorStr = "random-error".local
            }
            
            if let err = errorStr {
                
                if let failHandler = failure {
                    
                    failHandler(err)
                } else {
                    
                    showErrorMessage(err)
                }
            }
        }
    }
    
    typealias LoginSuccessHandler = (String, String, String?, String?, Bool) -> Void
    static func login(email:String, password:String, success:@escaping LoginSuccessHandler) {
        
        callApi(loadingComment: "\( "Logging".local )..",
                endpoint: "MemberLogin.ashx",
                params: ["email": email, "password": password],
                completion: { (json) -> String? in
                    
                    if let status = json["status"].string {
                        
                        if let userId = json["id"].string,
                            userId.trimmingCharacters(in: .whitespacesAndNewlines) != "0",
                            userId.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == false,
                            let name = json["name"].string {
                            
                            print(userId)
                            let photo = json["photo"].string
                            let date = json["date"].string
                            success(userId, name, photo, date, status == "success")
                            
                            return nil
                            
                        } else {
                            
                            return json["message"].string ?? "random-error".local
                        }
                        
                    } else {
                        
                        return json["message"].string ?? "random-error".local
                    }
        })
    }
    
    static func socialLogin(email:String, socialId:String, socialNetwork:Int, success:@escaping LoginSuccessHandler, notRegistered:@escaping () -> Void, failure:@escaping FailureHandler) {
        
        callApi(loadingComment: "\( "Logging".local )..",
            endpoint: "MemberLoginSocial.ashx",
            params: [ "email": email, "socialid": socialId, "socialnetwork": "\(socialNetwork)"],
            completion: { (json) -> String? in
                
                let errorMessage = json["message"].string ?? "random-error".local
                
                if let status = json["status"].string {
                    
                    if status == "success", let userId = json["id"].string, let name = json["name"].string {
                    
                        let photo = json["photo"].string
                        let date = json["date"].string
                        success(userId, name, photo, date, status == "success")
                        
                        return nil
                        
                    } else if status == "notregister" {
                        
                        notRegistered()
                        return nil
                        
                    } else {
                        
                        return errorMessage
                    }
                    
                } else {
                    
                    return errorMessage
                }
        }, failure: failure)
    }
    
    typealias ProfileSuccessHandler = (UserProfile?) -> Void
    static func fetchProfileForUser(_ userId:String, success:@escaping ProfileSuccessHandler) {
        
        let birthDF = DateFormatter()
        birthDF.locale = Locale(identifier: "en_US")
        birthDF.dateFormat = "dd/MM/yyyy"
        
        let regDF = DateFormatter()
        regDF.locale = Locale(identifier: "en_US")
        regDF.dateFormat = "dd/MM/yyyy HH:mm:ss"
        
        callApi(endpoint: "MemberView.ashx",
                params: ["id": userId],
                completion: { (loadedData) -> String? in
                    
                    if let json = loadedData["item"].array?.first {
                        
                        if let userId = json["id"].string,
                            let name = json["name"].string,
                            let phone = json["phone"].string,
                            let bdateStr = json["birthdate"].string, let bdate = birthDF.date(from: bdateStr),
                            let genStr = json["gender"].string,
                            let latStr = json["latitude"].string, let lat = Double(latStr),
                            let lonStr = json["longitude"].string, let lon = Double(lonStr),
                            let dateStr = json["date"].string, let regDate = regDF.date(from: dateStr) {
                            
                            
                            let gender = genStr.lowercased() == "true" ? 1 : 0
                            let coords = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                            let profile = UserProfile(id: userId,
                                                      name: name,
                                                      phone: phone,
                                                      birthdate: bdate,
                                                      gender: gender,
                                                      coords: coords,
                                                      regDate: regDate)
                            
                            profile.photoUrl = json["photo"].string
                            profile.address = json["address"].string
                            success(profile)
                            
                            return nil
                            
                        } else {
                            
                            return "Corrupted response"
                        }
                        
                    } else {
                        
                        return "random-error".local
                    }
        })
    }
    
    typealias SignUpSuccessHandler = (String, String?, String?) -> Void
    typealias SignUpProgressHandler = (Progress) -> Void
    static func signUp(name:String, email:String, password:String,
                       birthdate:Date, phone:String, countryCode:String, location:CLLocationCoordinate2D,
                       address:String?, gender:Int, photo:UIImage?,
                       success:@escaping SignUpSuccessHandler,
                       failure:FailureHandler? = nil) {
        
        let comment = "\("Registering".local)..."
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.show(withStatus: comment)
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateFormat = "dd-MM-yyyy"
        
        Alamofire.upload(multipartFormData: { (formData) in
            
            ["name": name,
                "email": email,
                "birthdate": formatter.string(from: birthdate),
                "latitude": "\(location.latitude)",
                "longitude": "\(location.longitude)",
                "address": address ?? "",
                "gender": "\(gender)",
                "phone": phone,
                "countrycode": countryCode,
                "fcmToken": UserDefaults.standard.string(forKey: "fcmToken") ?? "",
                "password": password ].forEach({ (key, value) in
                    formData.append(value.data(using: .utf8)!, withName: key)
                })
            
            if let image = photo {
                formData.append(UIImageJPEGRepresentation(image, 1)!,
                                withName: "photo",
                                fileName: "photo.jpg", mimeType: "image/jpeg")
            } else {
                
                formData.append("".data(using: .utf8)!, withName: "photo")
            }
            
        }, to: "\(base)/MemberAdd.ashx", encodingCompletion: createSignUpEncodingHandler(success: success, failure: failure) )
    }
    
    typealias SuccessCompletionHandler = () -> Void
    static func confirmSignup(forUser userId:String, withCode code:String, onSuccess:@escaping SuccessCompletionHandler) {
        
        let lang = UserDefaults.languageCode ?? "en"
        let langVal = lang == "en" ? 0 : 1
        
        callApi(endpoint: "MemberConfirm.ashx", params: ["id": userId, "confirm": code, "language": langVal], completion: { (json) -> String? in
            
            if let status = json["status"].string, status == "success" {
                onSuccess()
                return nil
            } else {
                
                return json["message"].string ?? "random-error".local
            }
        })
    }
    
    static func sendConfirmationCode(forUser userId:String, onSuccess:@escaping SuccessCompletionHandler) {
        
        callApi(endpoint: "MemberSendConfirmCode.ashx", params: ["id": userId], completion: { (json) -> String? in
            
            if let status = json["status"].string, status == "success" {
                onSuccess()
                return nil
            } else {
                return json["message"].string ?? "random-error".local
            }
        })
    }
    
    static func reportForgotPassword(forEmail email:String, onSuccess:@escaping SuccessCompletionHandler) {
        
        callApi(endpoint: "MemberResetPassword.ashx", params: ["email": email], completion: { (json) -> String? in
            
            if let status = json["status"].string, status == "success" {
                onSuccess()
                return nil
            } else {
                return json["message"].string ?? "random-error".local
            }
        })
    }
    
    static func submitVisit(forMember memberId:String) {
        
        Alamofire.request("\(base)/MemberVisit.ashx", method: .post, parameters: ["id": memberId]).responseData { (res) in
            print("Visit submitted: \(res.result.isSuccess)")
        }
    }
    
    static func checkVerification(forMember memberId:String, success:@escaping (Bool) -> Void, failure:@escaping (String, Bool) -> Void) -> DataRequest {
        
        let req = Alamofire.request("\(base)/MemberIdConfirm.ashx", method: .post, parameters: ["memberid": memberId])
        req.responseSwiftyJSON { (res) in
                
                switch res.result {
                case .success(let data):
                    
                    if let confirm = data["confirm"]
                        .string?.lowercased()
                        .trimmingCharacters(in: .whitespacesAndNewlines) {
                        
                        success(confirm == "true")
                    } else {
                        
                        failure(data["message"].string ?? "random-error".local, false)
                    }
                    
                case .failure(let err):
                    
                    let error = err as NSError
                    let isConnection:Bool
                    
                    switch error.code {
                    case NSURLErrorNotConnectedToInternet,
                         NSURLErrorCannotConnectToHost,
                         NSURLErrorNetworkConnectionLost,
                         NSURLErrorTimedOut,
                         NSURLErrorSecureConnectionFailed:
                        
                        isConnection = true
                    default:
                        isConnection = false
                    }
                    
                    failure(err.localizedDescription, isConnection)
                }
        }
        
        return req
    }
    
    typealias ProfileUpdateSuccessHandler = (String?) -> Void
    static func updateUserProfile(userId:String, name:String,
                       birthdate:Date, phone:String, location:CLLocationCoordinate2D,
                       address:String?, gender:Int, photo:UIImage?, removePhoto:Bool,
                       success:@escaping ProfileUpdateSuccessHandler,
                       failure:@escaping FailureHandler) {
        
        let comment = "\("Updating".local)..."
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.show(withStatus: comment)
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateFormat = "dd-MM-yyyy"
        
        Alamofire.upload(multipartFormData: { (formData) in
            
            ["id": userId,
                "name": name,
                "birthdate": formatter.string(from: birthdate),
                "latitude": "\(location.latitude)",
                "longitude": "\(location.longitude)",
                "address": address ?? "",
                "gender": "\(gender)",
                "deletephoto": "\(removePhoto ? 1 : 0)",
                "phone": phone ].forEach({ (key, value) in
                    
                    print("\(key): \(value)")
                    formData.append(value.data(using: .utf8)!, withName: key)
                })
            
            if let image = photo {
                formData.append(UIImageJPEGRepresentation(image, 1)!,
                                withName: "photo",
                                fileName: "photo.jpg", mimeType: "image/jpeg")
            } else {
                
                formData.append("".data(using: .utf8)!, withName: "photo")
            }
            
        }, to: "\(base)/MemberUpdate.ashx") { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.responseSwiftyJSON { response in
                    
                    SVProgressHUD.dismiss()
                    
                    print( String(data: response.data ?? Data(), encoding: .utf8 )! )
                    
                    if let json = response.result.value {
                        
                        if let status = json["status"].string, status == "success" {
                            
                            success(json["photo"].string)
                        } else {
                            
                            failure(json["message"].string ?? "random-error".local)
                        }
                        
                    } else {
                        
                        failure(response.error?.localizedDescription ?? "random-error".local)
                    }
                }
                
            case .failure(let encodingError):
                
                SVProgressHUD.dismiss()
                failure(encodingError.localizedDescription)
            }
        }
    }
    
    private static func fetchPlaces(endpoint:String, location : CLLocationCoordinate2D, keyword:String? = nil, category:String? = nil, completion:@escaping ([WebPlace]?) -> Void) {
        
        let user = AppUserInfo.current.userId ?? ""
        let lang = UserDefaults.languageCode ?? "en"
        let langVal = lang == "en" ? 0 : 1
        
        let parameters : [String : Any ] = [
            "keyword" : keyword ?? "",
            "location": "\(location.latitude),\(location.longitude)",
            "radius" : AppUserInfo.current.searchRadius / 1000,
            "category": category ?? "",
            "language": langVal,
            "memberid": user
        ]
        
        Alamofire.request("\(base)/\(endpoint)", method: .post, parameters: parameters).responseSwiftyJSON { response in
            
            if let items = response.result.value?["item"].array {
                
                var places = [WebPlace]()
                for item in items {
                    
                    if let id = item["id"].string,
                        let branchId = item["branchid"].string,
                        let name = item["name"].string,
                        let branchName = item["branchname"].string,
                        let categoryid = item["categoryid"].string,
                        let latStr = item["latitude"].string, let lat = Double(latStr),
                        let lonStr = item["longitude"].string, let lon = Double(lonStr) {
                        
                        
                        let place = WebPlace(id: id,
                                             coords: CLLocationCoordinate2D(latitude: lat, longitude: lon),
                                             name: name, type: categoryid, source: WebPlaceSource.server )
                        
                        place.category = PlaceCategories.shared.category(forId: categoryid)
                        place.subtitle = item["subtitle"].string
                        place.branchId = branchId
                        place.branchName = branchName
                        place.photoRef = item["logo"].string
                        place.serverGooglePlaceId = item["googleplaceid"].string
                        
                        places.append(place)
                    }
                }
                
                completion(places.count > 0 ? places : nil)
                
            } else {
                
                print (response.error!)
                completion(nil)
            }
        }
    }
    
    static func fetchPlacesWithOffers(location : CLLocationCoordinate2D, keyword:String? = nil, category:String? = nil, completion:@escaping ([WebPlace]?) -> Void) -> Void {
        
        fetchPlaces(endpoint: "PlacesOffers.ashx", location: location, keyword: keyword, category: category, completion: completion)
    }
    
    static func searchPlaces(location : CLLocationCoordinate2D, keyword:String? = nil, category:String? = nil, completion:@escaping ([WebPlace]?) -> Void) -> Void {
        
        fetchPlaces(endpoint: "Places.ashx", location: location, keyword: keyword, category: category, completion: completion)
    }
    
    static func fetchRemoteCategories(long:Bool = false, completion:@escaping ([PlaceCategory]?) -> Void) ->Void {
        
        let lang = UserDefaults.languageCode ?? "en"
        let langVal = lang == "en" ? 0 : 1
        
        do {
            
            var request = URLRequest(url: URL(string: "\(base)/categories.ashx")!)
            request.httpMethod = "POST"
            request.timeoutInterval = long ? 60 : 10
            
            request = try URLEncoding.default.encode(request, with: ["language": langVal])
            
            Alamofire.request(request).responseSwiftyJSON { (response) in
                
                var places:[PlaceCategory]?
                if let items = response.result.value?["item"].array {
                    
                    places = []
                    for item in items {
                        
                        if let id = item["id"].string,
                            let name = item["name"].string,
                            let photo = item["photo"].string, let iconUrl = URL(string: photo) {
                            
                            places?.append( PlaceCategory(id: id, name: name, remoteIcon: iconUrl) )
                        }
                    }
                }
                
                completion(places)
            }
            
        } catch {
            
            print("Failed to encode language ...")
        }
    }
    
    typealias PlaceDetailsSuccessHandler = (PlaceInfo, [String]?) -> Void
    static func getPlaceDetails(_ place:WebPlace, onSuccess:@escaping PlaceDetailsSuccessHandler){
        
        let user = AppUserInfo.current.userId ?? ""
        let lang = UserDefaults.languageCode ?? "en"
        let langVal = lang == "en" ? 0 : 1
        
        callApi(endpoint: "PlaceDetails.ashx", params: [ "id" : place.placeID, "branchid": place.branchId!, "language":  langVal, "memberid": user],
                completion: { (json) -> String? in
            
            if let details = json["item"].array?.first {
                
                print(details)
                
                if let placeID = details["id"].string,
                    let branchID = details["branchid"].string,
                    let latStr = details["latitude"].string, let lat = Double(latStr),
                    let lonStr = details["longitude"].string, let lon = Double(lonStr),
                    let name = details["name"].string,
                    let branchName = details["branchname"].string,
                    let categoryId = details["category"].string,
                    let category = PlaceCategories.shared.category(forId: categoryId) {
                    
                    let coords = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                    let info = PlaceInfo(id: placeID, name: name,
                                         branchId: branchID,
                                         branchName: branchName,
                                         category: category, coords: coords)
                    
                    
                    info.subtitle = details["subtitle"].string
                    info.about = details["about"].string
                    info.phoneNumber = details["phone"].string
                    info.logoUrl = details["logo"].string
                    
                    if let days = details["open_days"].dictionary?.mapValues({ (day) -> String in
                        return day.string!
                    }) {
                        info.openingDays = days
                    }
                    
                    let photos = details["photos"].array?.map({ (photo) -> String in
                        return photo.string!
                    })
                    
                    if let offers = details["offers"].array {
                        
                        for offerData in offers {
                            
                            if let text = offerData["offer"].string,
                                let from = offerData["from"].string,
                                let to = offerData["to"].string {
                                
                                info.offers.append(PlaceOffer(text: text, from: from, to: to))
                            }
                        }
                    }
                    
                    onSuccess(info, photos)
                    return nil
                    
                } else {
                    
                    return "Corrupted response"
                }
                
            } else {
                
                return "Corrupted response"
            }
        })
    }
    
    typealias AdsItemsSuccessHandler =  ([AdItem]?) -> Void
    static func fetchAdsItems(forCategory adCategoryId:String, andMember memberId:String, onSuccess:@escaping AdsItemsSuccessHandler, onFailure:@escaping FailureHandler) {
        
        fetchAdsItems(endpoint: "ads.ashx",
                      forParams: [
                        "categoryid": adCategoryId,
                        "memberid" : memberId
            ], onSuccess: onSuccess, onFailure: onFailure)
    }
    
    static func fetchAdsItems(forCategory adCategoryId:String, andVisitor fcmToken:String, onSuccess:@escaping AdsItemsSuccessHandler, onFailure:@escaping FailureHandler) {
        
        fetchAdsItems(endpoint: "adsvisitor.ashx",
                      forParams: [
                        "categoryid": adCategoryId,
                        "fcmtoken" : fcmToken
            ], onSuccess: onSuccess, onFailure: onFailure)
    }
    
    static func fetchAdsItems(endpoint:String, forParams params:[String:Any], onSuccess:@escaping AdsItemsSuccessHandler, onFailure:@escaping FailureHandler) {
        
        let lang = UserDefaults.languageCode ?? "en"
        let langVal = lang == "en" ? 0 : 1
        
        var fullParams = params
        fullParams["language"] = langVal
        
        Alamofire.request("\(base)/\(endpoint)", method: .post, parameters:fullParams).responseSwiftyJSON { response in
            
            if let items = response.result.value?["item"].array {
                
                var adsItems = [AdItem]()
                
                for item in items {
                    
                    adsItems.append( AdItem(text: item["adstext"].string,
                                            photo: item["photo"].string,
                                            date: item["date"].string) )
                }
                
                onSuccess( adsItems )
                
            } else {
                
                onFailure(response.error?.localizedDescription ?? "Corrupted response")
            }
        }
    }
    
    static func fetchAdsCategories(forMember memberId:String?, andUserFCMToken token:String?, onSuccess:@escaping ([AdCategory]?) -> Void, onFailure:@escaping FailureHandler) {
        
        let lang = UserDefaults.languageCode ?? "en"
        let langVal = lang == "en" ? 0 : 1
        var params:[String:Any] = ["language": langVal]
        
        if let memId = memberId {
            params["memberid"] = memId
        } else {
            params["fcmtoken"] = token ?? ""
        }
        
        Alamofire.request("\(base)/adscategories.ashx", method: .post, parameters:params, encoding: URLEncoding.default)
            .responseSwiftyJSON { response in
            
            if let items = response.result.value?["item"].array {
                
                var categories = [AdCategory]()
                
                for item in items {
                    
                    if let id = item["id"].string,
                       let name = item["name"].string,
                        let photo = item["photo"].string {
                        
                        categories.append( AdCategory(id: id, name: name, image: photo ) )
                    }
                }
                
                onSuccess( categories )
                
            } else {
                
                onFailure(response.error?.localizedDescription ?? "Corrupted response")
            }
        }
    }
    
    static func submitToken(_ fcmToken:String, forMember memberId:String) {
        //Fail gracefully
        Alamofire.request("\(base)/MemberUpdateToken.ashx", method: .post, parameters: ["fcmToken": fcmToken, "id": memberId])
    }
    
    static func updateToken(forMember memberId:String) {
        if let fcmToken = UserDefaults.standard.string(forKey: "fcmToken") {
            self.submitToken(fcmToken, forMember: memberId)
        }
    }
    
    static func submitVisitor(withToken fcmToken:String) {
        //Fail gracefully
        Alamofire.request("\(base)/VisitorAdd.ashx", method: .post, parameters: ["fcmToken": fcmToken])
    }
    
    static func socialSignUp(name:String, email:String, socialId:String, socialNetwork:Int,
                             birthdate:Date, phone:String, countryCode:String, location:CLLocationCoordinate2D,
                       address:String?, gender:Int, photo:UIImage?,
                       success:@escaping SignUpSuccessHandler,
                       failure:FailureHandler? = nil) {
        
        let comment = "\("Registering".local)..."
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.show(withStatus: comment)
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateFormat = "dd-MM-yyyy"
        
        Alamofire.upload(multipartFormData: { (formData) in
            
            ["name": name,
             "email": email,
             "birthdate": formatter.string(from: birthdate),
             "latitude": "\(location.latitude)",
                "longitude": "\(location.longitude)",
                "address": address ?? "",
                "gender": "\(gender)",
                "phone": phone,
                "countrycode": countryCode,
                "fcmToken": UserDefaults.standard.string(forKey: "fcmToken") ?? "",
                "socialid": socialId,
                "socialnetwork": "\(socialNetwork)" ].forEach({ (key, value) in
                    
                    print("\(key): \(value)")
                    formData.append(value.data(using: .utf8)!, withName: key)
                })
            
            if let image = photo {
                formData.append(UIImageJPEGRepresentation(image, 1)!,
                                withName: "photo",
                                fileName: "photo.jpg", mimeType: "image/jpeg")
            } else {
                
                formData.append("".data(using: .utf8)!, withName: "photo")
            }
            
        }, to: "\(base)/MemberAddSocial.ashx", encodingCompletion: createSignUpEncodingHandler(success: success, failure: failure) )
    }
    
    private static func createSignUpEncodingHandler(success:@escaping SignUpSuccessHandler, failure:FailureHandler? = nil)
        -> (SessionManager.MultipartFormDataEncodingResult) -> Void {
        
        return { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.responseSwiftyJSON { response in
                    
                    SVProgressHUD.dismiss()
                    if let json = response.result.value {
                        
                        if let status = json["status"].string, status == "success",
                            let userId = json["id"].string {
                            
                            success(userId, json["photo"].string, json["date"].string)
                        } else {
                            
                            failure?(json["message"].string ?? "random-error".local)
                        }
                        
                    } else {
                        
                        failure?(response.error?.localizedDescription ?? "random-error".local)
                    }
                }
                
            case .failure(let encodingError):
                
                SVProgressHUD.dismiss()
                failure?(encodingError.localizedDescription)
            }
        }
    }
}

class PlaceOffer {
    
    var text:String
    var from:String
    var to:String
    
    init(text:String, from:String, to:String) {
        
        self.text = text
        self.from = from
        self.to = to
    }
}

class PlacePhotoData {
    
    typealias PhotoLoadingCompletion = (UIImage?) -> Void
    
    var urlPath:String?
    var metaData:GMSPlacePhotoMetadata?
    
    init(meta:GMSPlacePhotoMetadata? = nil, url:String? = nil) {
        self.metaData = meta
        self.urlPath = url
    }
    
    var _image:UIImage?
    private var isLoading = false
    private var currentCompletionBlock:PhotoLoadingCompletion?
    func load(_ completion:@escaping (UIImage?) -> Void) -> Void {
        
        if let img = self._image {
            completion(img)
            return
        }
        
        self.currentCompletionBlock = completion
        
        if self.isLoading {
            return
        }
        
        if let url = self.urlPath {
            
            Alamofire.request(url).responseImage { (resp) in
                self._image = resp.result.value
                self.currentCompletionBlock?(self._image)
                self.currentCompletionBlock = nil
                self.isLoading = false
            }
        }
        
        if let photoMeta = self.metaData {
            
            GMSPlacesClient.shared().loadPlacePhoto(photoMeta) { (image, error) in
                DispatchQueue.main.async {
                    self._image = image
                    self.currentCompletionBlock?(image)
                    self.currentCompletionBlock = nil
                    self.isLoading = false
                }
            }
        }
    }
}

class PlaceInfo {
    
    unowned var category:PlaceCategory
    
    var branchId:String
    var branchName:String
    var placeID:String
    var coords:CLLocationCoordinate2D
    var name:String
    
    var logoUrl:String?
    var subtitle:String?
    var about:String?
    var phoneNumber:String?
    var offers:[PlaceOffer] = []
    var openingDays:[String:String] = [:]
    
    init(id:String, name:String, branchId:String, branchName:String, category:PlaceCategory, coords:CLLocationCoordinate2D) {
        
        self.placeID = id
        self.name = name
        self.category = category
        self.coords = coords
        self.branchId = branchId
        self.branchName = branchName
    }
    
    var isOpened:Bool {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "EEEE"
        
        let dayKey = dateFormatter.string(from: Date()).lowercased()
        
        guard let hoursStr = self.openingDays[dayKey],
            hoursStr.trimmingCharacters(in: .whitespaces) != "close"
            else {
                
            return false
        }
        
        let hoursRange = hoursStr.split(separator: "-").map({ (str) -> String in
            return str.trimmingCharacters(in: .whitespacesAndNewlines)
        })
        
        let hour1 = hoursRange[0]
        let hour2 = hoursRange[1]
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "HH:mm"
        
        let date1 = dateFormat.date(from: hour1)!
        let date2 = dateFormat.date(from: hour2)!
        
        let minutes1 = Calendar.current.component(.minute, from: date1) + Calendar.current.component(.hour, from: date1) * 60
        var minutes2 = Calendar.current.component(.minute, from: date2) + Calendar.current.component(.hour, from: date2) * 60
        minutes2 = (minutes2 < minutes1) ? minutes2 + 24 * 60 : minutes2
        
        let now = Date()
        let nowMinutes = Calendar.current.component(.minute, from: now) + Calendar.current.component(.hour, from: now) * 60
        
        return nowMinutes >= minutes1 && nowMinutes < minutes2
    }
}


class PlaceCategories {
    
    static let shared = PlaceCategories()
    private init() {}
    
    func category(forId id:String) -> PlaceCategory? {
        return self.items.first(where: { (cat) -> Bool in
            return cat.id == id
        })
    }
    
    var items:[PlaceCategory] = []
    private func downloadIcons(completion:@escaping () -> Void ) {
        
        let group = DispatchGroup()
        
        for category in self.items {
            
            if category.iconLocalUrl == nil {
                
                group.enter()
                Alamofire.request(category.iconRemoteUrl).responseImage { (response) in
                    
                    if let image = response.result.value, let data = UIImagePNGRepresentation(image) {
                        
                        if let docURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                            
                            do {
                                let imgUrl = docURL.appendingPathComponent("icon_\(category.id).png")
                                try data.write(to: imgUrl )
                                category.iconLocalUrl = imgUrl
                            } catch {}
                        }
                    }
                }
            }
        }
        
        group.notify(queue: DispatchQueue.main, execute: completion)
    }
    
    func reload(completion:@escaping () -> Void ) -> Void {
        
        let localItems = UserDefaults.standard.array(forKey: "categories")?.map({ (dict) -> PlaceCategory in
            return PlaceCategory(dict as! [String:Any])
        }) ?? []
        
        AppWebApi.fetchRemoteCategories(long: localItems.count == 0) { (remoteCategories) in
            
            if let categories = remoteCategories, categories.count > 0 {
                
                self.items = categories
                
                for cat in categories {
                    
                    if let locCat = localItems.first(where: { (locCat) -> Bool in return locCat.id == cat.id }) {
                        if cat.iconRemoteUrl == locCat.iconRemoteUrl {
                            cat.iconLocalUrl = locCat.iconLocalUrl
                        }
                    }
                }
                
            } else {
                
                self.items = localItems
            }
            
            self.downloadIcons(completion: {})
            completion()
        }
        
        
    }
}

class PlaceCategory {
    
    var id:String
    var name:String
    var iconRemoteUrl:URL
    var iconLocalUrl:URL?
    
    init(id:String, name:String, remoteIcon:URL) {
        self.id = id
        self.name = name
        self.iconRemoteUrl = remoteIcon
    }
    
    init(_ dict: [String:Any] ) {
        
        self.id = dict["id"] as! String
        self.name = dict["name"] as! String
        self.iconRemoteUrl = dict["iconRemoteUrl"] as! URL
        self.iconLocalUrl = dict["iconLocalUrl"]! as? URL
    }
    
    var dictionary:[String:Any] {
        
        return  [
            "id": self.id,
            "name": self.name,
            "iconRemoteUrl": self.iconRemoteUrl,
            "iconLocalUrl": self.iconLocalUrl ?? "null"
        ]
    }
}

class UserProfile {
    
    var id:String
    var name:String
    var phone:String
    var birthdate:Date
    var gender:Int
    var coords:CLLocationCoordinate2D
    var registrationDate:Date
    
    var address:String?
    var photoUrl:String?
    
    init(id:String, name:String, phone:String, birthdate:Date, gender:Int, coords:CLLocationCoordinate2D, regDate:Date) {
        self.id = id
        self.name = name
        self.phone = phone
        self.birthdate = birthdate
        self.gender = gender
        self.coords = coords
        self.registrationDate = regDate
    }
}



