//
//  Constants.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/9/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit

struct NotificationNames {
    
    static let ShowSideMenu = Notification.Name("ShowSideMenu")
    static let AppDidBecomeActive = Notification.Name("AppDidBecomeActive")
    static let AppLaunchUsingAdCategoryNotification = Notification.Name("AppLaunchUsingAdCategoryNotification")
}

struct GoogleKeys {
    static let clientID = "678140919176-kt0pmqku3snrj9arsrbb21qhpvatu4ro.apps.googleusercontent.com"
}

struct Colors {
    
    static let main = UIColor(red: 180/255.0, green: 149/255.0, blue: 79/255.0, alpha: 1)
}
