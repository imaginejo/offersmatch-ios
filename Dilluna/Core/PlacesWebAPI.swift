//
//  PlacesAPI.swift
//  OffersMatch
//
//  Created by Ali Hajjaj on 3/14/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON
import GoogleMaps

enum WebPlaceSource:Int {
    case google = 1
    case server = 0
}

class WebPlace {
    
    private var _category:PlaceCategory?
    var category:PlaceCategory? {
        
        set { self._category = newValue }
        get {
            return self._category ?? PlaceCategories.shared.category(forId: self.type)
        }
    }
    
    var placeID:String
    var coords:CLLocationCoordinate2D
    var name:String
    var type:String
    var photoRef:String?
    var vicinity:String?
    var subtitle:String?
    var isOpened:Bool = false
    var openHours:String?
    var source:WebPlaceSource
    var iconUrl:String?
    
    var serverGooglePlaceId:String?
    
    var branchName:String!
    var branchId:String!
    
    init(id:String, coords:CLLocationCoordinate2D, name:String, type:String, source:WebPlaceSource) {
        
        self.placeID = id
        self.coords = coords
        self.name = name
        self.type = type
        self.source = source
    }
    
    convenience init?(fromDictionary dictionary:[String:Any]) {
        
        if let id = dictionary["placeID"] as? String,
            let name = dictionary["name"] as? String,
            let type = dictionary["type"] as? String,
            let sourceRaw = dictionary["source"] as? Int,
            let source = WebPlaceSource(rawValue: sourceRaw),
            let coords = dictionary["coords"] as? [String:Double] {
            
            let loc = CLLocationCoordinate2D(latitude: coords["lat"]!, longitude: coords["lon"]!)
            self.init(id: id, coords: loc, name: name, type: type, source: source)
            self.subtitle = dictionary["subtitle"] as? String
            self.photoRef = dictionary["photoRef"] as? String
            self.vicinity = dictionary["vicinity"] as? String
            self.isOpened = dictionary["isOpened"] as! Bool
            self.openHours = dictionary["openHours"] as? String
            
            self.branchId = dictionary["branchid"] as? String
            self.branchName = dictionary["branchname"] as? String
            
        } else {
            
            return nil
        }
    }
    
    func dictionary() -> [String:Any] {
        
        return [
            "placeID": self.placeID,
            "name": self.name,
            "type": self.type,
            "subtitle": self.subtitle ?? false,
            "coords": [
                "lat": self.coords.latitude,
                "lon": self.coords.longitude,
            ],
            "photoRef": self.photoRef ?? false,
            "vicinity": self.vicinity ?? false,
            "isOpened": self.isOpened,
            "openHours": self.openHours ?? false,
            "source": self.source.rawValue,
            "branchid": self.branchId ?? false,
            "branchname": self.branchName ?? false
        ]
    }
    
    var distance:Double {
        return calculateDistance(loc1: LocationService.shared.certainLocation, loc2: self.coords)
    }
    
    var distanceString:String {
        
        return "\( (self.distance / 1000).formatted(2)! ) \( "km".local ) "
    }
    
    private var _directionString:String?
    func getDirectionDistance(completion:@escaping (String) -> Void) -> Void {
        
        if let dist = self._directionString {
            completion(dist)
            return
        }
        
        DirectionsWebAPI.bestRoute(origin: LocationService.shared.certainLocation,
                                   destination: self.coords, mode: .driving)
        { (route) in
            
            let distStr:String
            if let distance = route?.totalDistance {
                distStr = "\( ( distance / 1000 ).formatted(2)! ) \( "km".local )"
                self._directionString = distStr
            } else {
                distStr = self.distanceString
            }
            
            completion( distStr )
        }
    }
}

class PlacesWebAPI {
    
    private static let apiUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
    static let apiKey = "AIzaSyBTiQXhLKD4K4CR0FRGplXNsvArpk0CNCI"
    
    static func getPlacesNearby(_ location:CLLocationCoordinate2D, type:String? = nil, keyword:String? = nil, completion:@escaping ([WebPlace]?) -> Void) -> Void {
        
        var params:[String:Any] = [
            "key": apiKey,
            "location": "\(location.latitude),\(location.longitude)",
            "radius": AppUserInfo.current.searchRadius
        ]
        
        if let _type = type {
            params["type"] = _type
        }
        
        if let _keyword = keyword {
            params["keyword"] = _keyword
        }
        
        let req = Alamofire.request(apiUrl, method: .get, parameters: params)
        debugPrint(req)
        req.responseSwiftyJSON { (response) in
            
            debugPrint(response.result.value ?? "")
            
            var places:[WebPlace]?
            if let result = response.result.value?["results"].array {
                
                places = []
                for item in result {
                    
                    if let id = item["place_id"].string,
                        let name = item["name"].string,
                        let coords = item["geometry"]["location"].dictionary,
                        let lat = coords["lat"]?.double,
                        let lon = coords["lng"]?.double,
                        let jtypes = item["types"].array {
                        
                        let types = jtypes.map({ (json) -> String in return json.string ?? "" })
                        let place = WebPlace(id: id,
                                             coords: CLLocationCoordinate2D(latitude: lat, longitude: lon),
                                             name: name, type: types.first!, source: .google)
                        place.subtitle = types.first
                        place.vicinity = item["vicinity"].string
                        place.openHours = item["opening_hours"]["weekday_text"].array?
                            .reduce("", { (result, dayText) -> String in
                                return result + ", " + dayText.string!
                            })
                        
                        place.isOpened = item["opening_hours"]["open_now"].bool ?? false
                        place.photoRef = item["photos"].array?.first?["photo_reference"].string
                        place.iconUrl = item["icon"].string
                        places?.append(place)
                    }
                }
                
            } else if let error = response.error {
                
                showErrorMessage(error.localizedDescription)
            }
            
            completion(places)
        }
    }
    
    static func loadPlacePhoto(photoRef:String, maxWidth:Int = 100, completion:@escaping (UIImage?) -> Void) -> DataRequest {
        
        let urlPath = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=\(maxWidth)&photoreference=\(photoRef)&key=\(apiKey)"
        
        return Alamofire.request(urlPath).responseImage { (response) in
            completion(response.result.value)
        }
    }
}

enum TravelMode:String {
    case driving = "driving"
    case walking = "walking"
    case bicycling = "bicycling"
    case transit = "transit"
}

class DirectionsWebAPI {
    
    class func bestRoute(origin:CLLocationCoordinate2D, destination:CLLocationCoordinate2D, mode:TravelMode, completion:@escaping (WebRoute?) -> Void) {
        
        bestRoute(throughPoints: [origin, destination], mode: mode, completion: completion)
    }
    
    class func bestRoute(throughPoints points:[CLLocationCoordinate2D], mode:TravelMode, completion:@escaping (WebRoute?) -> Void ) -> Void {
        
        if points.count >= 2 {
            
            let fp = points.first!
            let ep = points.last!
            
            let url = "https://maps.googleapis.com/maps/api/directions/json"
            var params:[String:Any] = [
                "origin": "\(fp.latitude),\(fp.longitude)",
                "destination": "\(ep.latitude),\(ep.longitude)",
                "key": PlacesWebAPI.apiKey,
                "mode": mode.rawValue
            ]
            
            if points.count > 2 {
                
                var waypoints = [String]()
                for point in points[1 ..< (points.count - 1) ] {
                    waypoints.append("\(point.latitude),\(point.longitude)")
                }
                
                params["waypoints"] = "optimize:true|\(waypoints.joined(separator: "|"))"
            }
            
            Alamofire.request(url, method: .get, parameters: params)
                .responseSwiftyJSON(completionHandler: { (response) in
                    
                    if let json = response.result.value {
                        
                        debugPrint(json)
                        
                        if let pathStr = json["routes"][0]["overview_polyline"]["points"].string,
                            let path = GMSPath(fromEncodedPath: pathStr) {
                            
                            let legs = json["routes"][0]["legs"].array?.map({ (leg) -> WebRouteLeg in
                                
                                return WebRouteLeg(mode: mode,
                                                   distance: leg["distance"]["value"].double! ,
                                                   duration: leg["duration"]["value"].double! )
                            }) ?? []
                            
                            completion( WebRoute(path, legs: legs) )
                            return
                        }
                    }
                    
                    completion(nil)
                })
            
        }
    }
}

struct WebRouteLeg {
    var distance:Double
    var duration:Double
    var mode:TravelMode
    
    init(mode:TravelMode, distance:Double, duration:Double) {
        self.mode = mode
        self.distance = distance
        self.duration = duration
    }
}

class WebRoute {
    
    private(set) var totalDistance:Double
    var path:GMSPath
    var legs:[WebRouteLeg]
    
    init(_ path:GMSPath, legs:[WebRouteLeg]) {
        self.path = path
        self.legs = legs
        self.totalDistance = legs.reduce(0.0, { (dist, leg) -> Double in
            return dist + leg.distance
        })
    }
}
