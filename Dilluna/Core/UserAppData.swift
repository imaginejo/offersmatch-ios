//
//  UserAppData.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/15/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import Foundation
import GoogleSignIn
import FacebookCore
import FacebookLogin

enum PlaceNavigationOption : Int {
    case maps
    case uber
}

class AppUserInfo {
    
    static let current = AppUserInfo()
    
    var defaults:UserDefaults
    
    var isSignedIn:Bool {
        return UserDefaults.standard.string(forKey: "userId") != nil
    }
    
    var name:String? {
        set { self.defaults.set(newValue, forKey: "name") }
        get { return self.defaults.string(forKey: "name") }
    }
    
    var network:SocialNetwork? {
        set { self.defaults.set(newValue?.rawValue, forKey: "network") }
        get { return SocialNetwork(rawValue: self.defaults.integer(forKey: "network")) }
    }
    
    var registrationDate:String? {
        set { self.defaults.set(newValue, forKey: "registrationDate") }
        get { return self.defaults.string(forKey: "registrationDate") }
    }
    
    var email:String? {
        set { self.defaults.set(newValue, forKey: "email") }
        get { return self.defaults.string(forKey: "email") }
    }
    
    var photo:String? {
        set { self.defaults.set(newValue, forKey: "photo") }
        get { return self.defaults.string(forKey: "photo") }
    }
    
    var userId:String? {
        set { self.defaults.set(newValue, forKey: "userId") }
        get { return self.defaults.string(forKey: "userId") }
    }
    
    var isConfirmed:Bool {
        set { self.defaults.set(newValue, forKey: "isConfirmed") }
        get { return self.defaults.bool(forKey: "isConfirmed") }
    }
    
    var searchRadius:Double {
        set { self.defaults.set(newValue, forKey: "searchRadius") }
        get { return self.defaults.double(forKey: "searchRadius") }
    }
    
    var navigationOption:PlaceNavigationOption {
        
        set {
            self.defaults.set(newValue.rawValue, forKey: "navigationOption")
        }
        get {
            return PlaceNavigationOption(rawValue: self.defaults.integer(forKey: "navigationOption")) ?? .maps
        }
    }
    
    func logout() -> Void {
        self.defaults.removeObject(forKey: "userId")
        self.defaults.removeObject(forKey: "name")
        self.defaults.removeObject(forKey: "email")
        self.defaults.removeObject(forKey: "photo")
        self.defaults.removeObject(forKey: "isConfirmed")
        self.defaults.removeObject(forKey: "searchRadius")
        self.defaults.removeObject(forKey: "navigationOption")
        
        if let network = self.network {
            self.defaults.removeObject(forKey: "network")
            SocialLogin.logout(network)
        }
    }
    
    private init(){
        self.defaults = UserDefaults.standard
    }
}

class UserAppData {
    
    private class func addPlaceToList(_ place:WebPlace, key:String) -> Void {
        
        var tripsArray = UserDefaults.standard.array(forKey: key) as? [[String:Any]] ?? [[String:Any]]()
        
        let itemIndex = tripsArray.index(where: { (item) -> Bool in
            let id = item["placeID"] as! String
            return id == place.placeID
        })
        
        if let i = itemIndex {
            
            tripsArray[i] = place.dictionary()
            
        } else {
            
            tripsArray.append(place.dictionary())
        }
        
        UserDefaults.standard.set(tripsArray, forKey: key)
    }
    
    private class func removePlaceFromList(_ placeId:String, key:String) -> Void {
        
        var tripsArray = UserDefaults.standard.array(forKey: key) as? [[String:Any]] ?? [[String:Any]]()
        
        if let index = tripsArray.index(where: { (item) -> Bool in
            let id = item["placeID"] as! String
            return id == placeId
        }) {
            
            tripsArray.remove(at: index)
        }
        
        UserDefaults.standard.set(tripsArray, forKey: key)
    }
    
    private class func propertyItems(key:String) -> [WebPlace] {
        
        var items = [WebPlace]()
        if let plistArray = UserDefaults.standard.array(forKey: key) as? [[String:Any]] {
            
            for itemDict in plistArray {
                if let item = WebPlace(fromDictionary: itemDict) {
                    items.append(item)
                }
            }
        }
        
        return items
    }
    
    private class func isPlaceIncludedInList(_ placeId:String, forKey key:String) -> Bool {
        
        if let plistArray = UserDefaults.standard.array(forKey: key) as? [[String:Any]] {
            
            return plistArray.contains(where: { (item) -> Bool in
                let pId = item["placeID"] as! String
                return pId == placeId
            })
        }
        
        return false
    }
    
    class func isPlaceFavoritesIncluded(_ placeId:String) -> Bool {
        
        return self.isPlaceIncludedInList(placeId, forKey: "favoriteItems")
    }
    
    class func addPlaceToFavorite(_ place:WebPlace) -> Void {
        
        addPlaceToList(place, key:"favoriteItems")
    }
    
    class func removePlaceFromFavorite(_ place:WebPlace) -> Void {
        
        removePlaceFromList(place.placeID, key:"favoriteItems")
    }
    
    class func removePlaceFromFavorite(id:String) -> Void {
        
        removePlaceFromList(id, key:"favoriteItems")
    }
    
    class func favoriteItems() -> [WebPlace] {
        
        return propertyItems(key: "favoriteItems")
    }
}

