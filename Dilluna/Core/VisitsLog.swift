//
//  VisitsLog.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/15/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import Foundation
import TrueTime

class VisitsLog {
    
    static let shared = VisitsLog()
    
    var dictionary:[String:Bool]
    private let dateFormatter = DateFormatter()
    
    var days:[String] {
        return self.dictionary.keys.sorted(by: { (day1, day2) -> Bool in
            
            let date1 = self.dateFormatter.date(from: day1)!
            let date2 = self.dateFormatter.date(from: day2)!
            return date1 < date2
        })
    }
    
    func indexOf(_ date:Date) -> Int? {
        let dayKey = dateFormatter.string(from: date)
        return self.days.index(of: dayKey)
    }
    
    var dates:[Date] {
        
        return self.days.map({ (dayStr) -> Date in
            return self.dateFormatter.date(from: dayStr)!
        })
    }
    
    func checkApplicable(_ date:Date) -> Bool {
        
        let dates = self.dates
        if let first = dates.first, let last = dates.last {
            return date >= first && date <= last
        }
        return false
    }
    
    private init() {
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.locale = Locale(identifier: "en")
        
        if let _log = UserDefaults.standard.dictionary(forKey: "VisitsLog") as? [String:Bool] {
            self.dictionary = _log
        } else {
            self.dictionary = [:]
        }
    }
    
    func recreate() -> Void {
        
        self.dictionary.removeAll()
        
        if let refTime = TrueTimeClient.sharedInstance.referenceTime {
            
            for i in 0 ..< 30 {
                let date = refTime.now().addingTimeInterval(Double(i) * 24 * 60 * 60)
                let dayKey = dateFormatter.string(from: date)
                self.dictionary[dayKey] = false
            }
        }
        
        self.save()
    }
    
    func save() -> Void {
        
        UserDefaults.standard.set(self.dictionary, forKey: "VisitsLog")
    }
    
    func validate() -> Void {
        
        if let userId = UserDefaults.standard.string(forKey: "VisitsLog_UserID") {
            
            if userId != AppUserInfo.current.userId {
                UserDefaults.standard.set(userId, forKey: "VisitsLog_UserID")
                self.dictionary.removeAll()
            }
            
        } else {
            
            self.dictionary.removeAll()
        }
    }
    
    var isValid:Bool {
        return self.dictionary.count > 0
    }
    
    func markVisited(_ date:Date ) -> Void {
        
        UserDefaults.standard.set(AppUserInfo.current.userId, forKey: "VisitsLog_UserID")
        
        let dayKey = dateFormatter.string(from:  date)
        if let visited = self.dictionary[dayKey] {
            
            if !visited {
                
                print("First Visit")
                self.dictionary[dayKey] = true
                self.save()
            }
        }
        
        if let userId = AppUserInfo.current.userId {
            AppWebApi.submitVisit(forMember: userId)
        }
    }
    
    func checkVisited(_ dayKey:String ) -> Bool {
        
        if let visited = self.dictionary[dayKey] {
            return visited
        } else {
            return false
        }
    }
    
    func checkVisited( date:Date ) -> Bool {
        
        return self.checkVisited(dateFormatter.string(from:  date))
    }
}

