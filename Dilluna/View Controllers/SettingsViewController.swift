//
//  SettingsViewController.swift
//  Dilluna
//
//  Created by Suhayb Ahmad on 8/7/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController, RadiusViewControllerDelegate {
    
    private var checkIcon = #imageLiteral(resourceName: "baseline_check_black_24pt").withRenderingMode(.alwaysTemplate)

    @IBOutlet var langIcons: [UIImageView]!
    @IBOutlet var navIcons: [UIImageView]!
    
    @IBOutlet weak var radiusIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.radiusIcon.image = #imageLiteral(resourceName: "ic_my_location_18pt").withRenderingMode(.alwaysTemplate)
        
        if let lang = UserDefaults.languageCode {
            self.langIcons.forEach { $0.image = nil }
            self.langIcons[ lang == "en" ? 0 : 1 ].image = self.checkIcon
        }
        
        self.radiusLabel.text = formatRadius( AppUserInfo.current.searchRadius )
        self.checkNavigationOption( AppUserInfo.current.navigationOption )
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            
            if indexPath.row == 0 {
                self.switchToLanguage("en", region: "US")
            } else {
                self.switchToLanguage("ar", region: "JO")
            }
            
        case 1:
            self.openRadiusEditor()
        case 2:
            self.changeNavigationOption()
        default:
            break
        }
    }
    
    func openRadiusEditor() -> Void {
    
        RadiusViewController.presentOn(self)
    }
    
    func changeNavigationOption() -> Void {
        
        let option:PlaceNavigationOption =
            AppUserInfo.current.navigationOption == .maps ? .uber : .maps
        
        AppUserInfo.current.navigationOption = option
        self.checkNavigationOption(option)
    }
    
    func checkNavigationOption(_ option:PlaceNavigationOption ) -> Void {
        
        self.navIcons.forEach { $0.image = nil }
        self.navIcons[option == .maps ? 0 : 1].image = checkIcon
    }
    
    func switchToLanguage(_ lang:String, region:String ) {
        
        let title = lang == "ar" ? "Language Change" : "تغيير اللغة"
        let message = lang == "ar"
            ? "In order to change language, we need to terminate the app first. Then, you can run it again to see the effect"
            : "لتغيير اللغة، نحتاج لإغلاق التطبيق أولاً. بعد ذلك، تستطيع تشغيله مرة أخرى لرؤية التغيير"
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let title_proceed = lang == "ar" ? "OK" : "حسناً"
        let title_cancel = lang == "ar" ? "Cancel" : "إلغاء"
        
        let proceed = UIAlertAction(title: title_proceed, style: .default, handler: { (action) in
            LangChangeCloseViewController.launch(withLanguageCode: lang, andRegion: region)
        })
        
        let cancel = UIAlertAction(title: title_cancel, style: .destructive, handler: nil)
        alert.addAction(cancel)
        alert.addAction(proceed)
        
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
    }
    
    @IBOutlet weak var radiusLabel: UILabel!
    func radiusController(_ radiusVC: RadiusViewController, didSelectRadius radius: Double) {
        
        self.radiusLabel.text = formatRadius(radius)
    }
}
