//
//  dailyProgressViewController.swift
//  Dillluna
//
//  Created by AhmeDroid on 4/9/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import TrueTime

class DayProgressViewController: BaseViewController {

    @IBOutlet var dayAsAWord: UILabel!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var continueButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isSidMenuSwipeEnabled = false
        
        self.dayAsAWord.applyDarkShadow(opacity: 0.2333, offset: CGSize(width: 0 , height: 1 )   )
        self.dayLabel.applyDarkShadow(opacity: 0.2333, offset: CGSize(width: 0 , height: 1 ) )
        self.continueButton.layer.cornerRadius = 4
        
        guard let today = TrueTimeClient.sharedInstance.referenceTime?.now() else {
            if let navVC = self.navigationController as? MainNavigationController {
                navVC.showIntroVC(animated: true)
            }
            return
        }
        
        VisitsLog.shared.markVisited(today)
        
        let index = VisitsLog.shared.indexOf(today) ?? 0
        
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "en_US")
        numberFormatter.minimumIntegerDigits = 2
        
        self.dayLabel.text = numberFormatter.string(from: NSNumber(value: index + 1))
    }
}
