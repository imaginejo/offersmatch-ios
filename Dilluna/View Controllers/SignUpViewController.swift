//
//  SignUpViewController.swift
//  OffersMatch
//
//  Created by Ali Hajjaj on 3/14/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import Validator
import Alamofire
import AlamofireImage
import CoreLocation
import Firebase

class SignUpViewController: BaseViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var dialCodeView: DialCodeView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmField: UITextField!
    
    @IBOutlet weak var profilePhotoView: UIImageView!
    
    @IBOutlet var roundViews: [UIView]!
    var selectedImage:UIImage?
    var selectedGender:Int = 1  // 1: Male, 0: Female
    
    @IBOutlet var genderButtons: [UIButton]!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    
    let lightGrayColor = UIColor(white: 0.80, alpha: 1)
    let darkColor = UIColor(valueRed: 112, green: 144, blue: 171, alpha: 1)
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        }
        
        self.isSidMenuSwipeEnabled = false
        
        self.doSetSelectedImage(nil)
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillShow, object: nil, queue: nil) { (notif) in
            
            if let frame = notif.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
                self.bottomConst.constant = frame.height
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillHide, object: nil, queue: nil) { _ in
            
            self.bottomConst.constant = 0
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        self.roundViews.forEach { (view) in
            view.layer.cornerRadius = 3
            view.applyDarkShadow(opacity: 0.3, offsetY: 1)
        }
        
        self.genderButtons.forEach { (btn) in
            btn.layer.cornerRadius = 3
            btn.applyDarkShadow(opacity: 0.3, offsetY: 1)
        }
        
        self.styleGenderButtons(forSelection: self.selectedGender)
        self.birthdateButton.setTitle("Select Birth Date".local, for: .normal)
        self.locationButton.setTitle("Select Location".local, for: .normal)
        
        self.birthdateButton.setTitleColor(lightGrayColor, for: .normal)
        self.locationButton.setTitleColor(lightGrayColor, for: .normal)
    }
    
    @IBAction func viewDidTap(_ sender: Any) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func genderSelected(_ sender: Any) {
        
        let button = sender as! UIButton
        self.selectedGender = button.tag
        self.styleGenderButtons(forSelection: button.tag)
    }
    
    func styleGenderButtons(forSelection tagSelected:Int) -> Void {
        
        self.genderButtons.forEach { (btn) in
            
            if btn.tag == tagSelected {
                
                btn.backgroundColor = darkColor
                btn.tintColor = UIColor.white
                
            } else {
                
                btn.backgroundColor = UIColor.white
                btn.tintColor = darkColor
            }
        }
    }
    
    @IBOutlet weak var birthdateButton: UIButton!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var locationButton: UIButton!
    
    var selectedBirthDate:Date?
    var selectedLocation:CLLocationCoordinate2D?
    var selectedAddress:String?
    
    @IBAction func launchBirthdateSelection(_ sender: Any) {
        
        let tenYearsOld = Date(timeIntervalSinceNow: -10 * 365 * 24 * 3600)
        DatePickerViewController.launch(delegate: self, initialDate: self.selectedBirthDate, maximumDate: tenYearsOld)
    }
    
    @IBAction func launchLocationSelection(_ sender: Any) {
        
        LocationSelectViewController.launch(delegate: self)
    }
    
    func doSetSelectedImage(_ image:UIImage? ) -> Void {
        
        self.selectedImage = image
        self.profilePhotoView.image = image
    }
    
    @IBAction func startPhotoPicking(_ sender: Any) {
        
        var imageRemovalAction:ImageRemovalBlock?
        if self.selectedImage != nil {
            
            imageRemovalAction = {
                self.doSetSelectedImage(nil)
            }
        }
        
        launchImageSelection(onView: self, sourceView: sender as! UIView, onImageRemoval: imageRemovalAction)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            let size = CGSize(width: 250, height: 250)
            let photo = image.af_imageAspectScaled(toFill: size)
            self.doSetSelectedImage(photo)
        }
        
        picker.dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitSignup(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let reqRule = ValidationRuleRequired<String>(error: ValidationError.name)
        let emailRule = ValidationRulePattern(pattern: EmailValidationPattern.standard, error: ValidationError.email)
        
        let passRule = ValidationRuleLength(min: 6, max: 40, lengthType: .characters, error: ValidationError.password)
        let passMatch = ValidationRuleEquality<String>(dynamicTarget: { () -> String in
            return self.confirmField.text ?? ""
        }, error: ValidationError.confirm)
        
        guard let name = self.nameField.text, name.validate(rule: reqRule).isValid,
            let phone = self.phoneField.text, phone.validate(rule: reqRule).isValid,
            let birthdate = self.selectedBirthDate,
            let location = self.selectedLocation else {
            
            showErrorMessage("signup-error".local)
            return
        }
        
        guard let email = self.emailField.text, email.validate(rule: emailRule).isValid else {
            showErrorMessage("email-error".local)
            return
        }
        
        guard let pass = self.passwordField.text else {
            showErrorMessage("password-error".local)
            return
        }
        
        if pass.validate(rule: passRule).isValid == false {
            showErrorMessage("password-error-rule".local)
            return
        }
        
        if pass.validate(rule: passMatch).isValid == false {
            showErrorMessage("password-error-match".local)
            return
        }
        
        let fullPhone = "\(self.dialCodeView.value)\(phone)"
        
        AppWebApi.signUp(name: name,
                         email: email,
                         password: pass,
                         birthdate: birthdate,
                         phone: fullPhone,
                         countryCode: self.dialCodeView.countryCode,
                         location: location,
                         address: self.selectedAddress,
                         gender: self.selectedGender,
                         photo: self.selectedImage,
                         success: { (userId, photoUrl, date) in
            
            AppUserInfo.current.userId = userId
            AppUserInfo.current.name = name
            AppUserInfo.current.email = email
            AppUserInfo.current.photo = photoUrl
            AppUserInfo.current.registrationDate = date
            AppUserInfo.current.isConfirmed = false
                
            let ageComps = Calendar.current.dateComponents([.year], from: birthdate, to: Date())
            Analytics.setUserProperty(self.selectedGender == 1 ? "Male" : "Female", forName: "Gender")
            Analytics.setUserProperty("\(ageComps.year!)", forName: "Age")
            Analytics.logEvent(AnalyticsEventSignUp, parameters: [
                AnalyticsParameterSignUpMethod: "email"
            ])
            
            self.performSegue(withIdentifier: "signup_to_confirm", sender: nil)
            
        }, failure: { (error) in
            
            showErrorMessage(error)
        })
    }
    
}

extension SignUpViewController: DatePickerViewControllerDelegate, LocationSelectViewControllerDelegate {
    
    func datePickerControllerDidSelectDate(_ selectedDate: Date) {
        
        self.selectedBirthDate = selectedDate
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        
        self.birthdateButton.setTitle(dateFormatter.string(from: selectedDate), for: .normal)
        self.birthdateButton.setTitleColor(UIColor.black, for: .normal)
    }
    
    func datePickerControllerDidClearSelection(_ datePickerVC: DatePickerViewController) {
        
        self.selectedBirthDate = nil
        self.birthdateButton.setTitle("Select Birth Date".local, for: .normal)
        self.birthdateButton.setTitleColor(lightGrayColor, for: .normal)
    }
    
    func locationSelectDidReceiveAddress(_ address: String, atCoordinates coordinates: CLLocationCoordinate2D) {
        
        self.selectedLocation = coordinates
        self.selectedAddress = address
        
        self.dialCodeView.setLocation(coordinates)
        self.locationButton.setTitle(address, for: .normal)
        self.locationButton.setTitleColor(UIColor.black, for: .normal)
    }
}

typealias ImageRemovalBlock = () -> Void
func launchImageSelection<Type: UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate>(onView vc:Type, sourceView:UIView, onImageRemoval:ImageRemovalBlock? = nil ) -> Void {
    
    let actionSheet = UIAlertController(title: "pick-photo".local, message: "select-photo".local, preferredStyle: .actionSheet)
    let topVC = UIApplication.topViewController()!
    
    let camera = UIAlertAction(title: "camera".local, style: .default) { (action) in
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = vc
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            
            topVC.present(imagePicker, animated: true, completion: nil)
        } else {
            
            showErrorMessage("camera-inaccessible".local)
        }
    }
    
    let gallery = UIAlertAction(title: "photo-lib".local, style: .default) { (action) in
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = vc
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            
            topVC.present(imagePicker, animated: true, completion: nil)
        } else {
            
            showErrorMessage("library-inaccessible".local)
        }
    }
    
    let cancel = UIAlertAction(title: "cancel".local, style: .cancel, handler: nil)
    
    actionSheet.addAction(camera)
    actionSheet.addAction(gallery)
    
    if onImageRemoval != nil {
        
        let remove = UIAlertAction(title: "remove-photo".local, style: .destructive) { (action) in
            onImageRemoval?()
        }
        
        actionSheet.addAction(remove)
    }
    
    actionSheet.addAction(cancel)
    
    if let popoverVC = actionSheet.popoverPresentationController {
        popoverVC.sourceView = sourceView
        popoverVC.sourceRect = sourceView.bounds
    }
    
    UIApplication.topViewController()?.present(actionSheet, animated: true, completion: nil)
}



protocol ImageSelectorDelegate : class {
    func imageSelectorDidSelectImage(_ image:UIImage, fromSource source:UIImagePickerControllerSourceType) -> Void
}

func launchImageSelection2<Type: UIViewController & ImageSelectorDelegate>(onView vc:Type, source:UIImagePickerControllerSourceType, allowEditing:Bool = false) -> Void {
    ImageSelector.shared.launch(onView: vc, source: source, allowEditing: allowEditing)
}

fileprivate class ImageSelector: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    fileprivate static let shared = ImageSelector()
    private override init(){}
    
    private var currentSource:UIImagePickerControllerSourceType?
    private weak var currentDelegate:ImageSelectorDelegate?
    
    fileprivate func launch(onView vc:UIViewController, source:UIImagePickerControllerSourceType, allowEditing:Bool = false) -> Void {
        
        if UIImagePickerController.isSourceTypeAvailable(source) {
            
            self.currentSource = source
            self.currentDelegate = vc as? ImageSelectorDelegate
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = source
            imagePicker.allowsEditing = allowEditing
            
            vc.present(imagePicker, animated: true, completion: nil)
        } else {
            
            let errorMsg = source == .camera ? "camera-inaccessible".local : "library-inaccessible".local
            showErrorMessage(errorMsg)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.currentSource = nil
        self.currentDelegate = nil
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let key:String = picker.allowsEditing ? UIImagePickerControllerEditedImage : UIImagePickerControllerOriginalImage
        
        if let image = info[key] as? UIImage {
            self.currentDelegate?.imageSelectorDidSelectImage(image, fromSource: self.currentSource!)
        }
        
        self.currentSource = nil
        self.currentDelegate = nil
        picker.dismiss(animated:true, completion: nil)
    }
}
