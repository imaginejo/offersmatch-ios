//
//  LoadingViewController.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/16/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import TrueTime

class LoadingViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isSidMenuSwipeEnabled = false
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        PlaceCategories.shared.reload {
            
            let nav = self.navigationController as! MainNavigationController
            
            /*if let refTime = TrueTimeClient.sharedInstance.referenceTime {
                
                self.processTime( refTime )
                
            } else {
                
                TrueTimeClient.sharedInstance.fetchIfNeeded(success: { (refTime) in
                    
                    self.processTime(refTime)
                    
                }, failure: { (error) in
                    
                    if LaunchCondition.current.isFirstLaunch {
                        nav.showFirstLaunchVC()
                    } else {
                        nav.showDayProgress()
                    }
                })
            }*/
            
            nav.showIntroVC(animated: true)
        }
    }
    
    func processTime(_ refTime:ReferenceTime) -> Void {
        
        let today = refTime.now()
        let nav = self.navigationController as! MainNavigationController
        
        if LaunchCondition.current.isFirstLaunch {
            
            nav.showFirstLaunchVC()
            
        } else if AppUserInfo.current.isSignedIn {
            
            if VisitsLog.shared.checkApplicable( today ) == false || VisitsLog.shared.checkVisited(date: today) {
                
                nav.showIntroVC(animated: false)
                
            } else {
                
                nav.showDayProgress()
            }
            
        } else {
            
            nav.showIntroVC(animated: false)
        }
    }
}
