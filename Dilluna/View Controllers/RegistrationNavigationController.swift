//
//  RegistrationNavigationController.swift
//  Dilluna
//
//  Created by Suhayb Ahmad on 5/6/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import TrueTime

class RegistrationNavigationController: UINavigationController {
    
    var currentProfile:UserProfile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if AppUserInfo.current.isSignedIn == false {
            
            self.showLogin(animated: false)
            
        } else if let profileVC = self.viewControllers.first as? ProfileViewController {
            
            profileVC.currentProfile = self.currentProfile
        }
    }
    
    func showLogin(animated:Bool) -> Void {
        
        if let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") {
            self.setViewControllers([loginVC], animated: animated)
        }
    }
    
    deinit {
        self.currentProfile = nil
    }
}
