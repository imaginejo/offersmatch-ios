//
//  FirstLaunchViewController.swift
//  Dilluna
//
//  Created by Suhayb Ahmad on 5/8/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import TrueTime

class FirstLaunchViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isSidMenuSwipeEnabled = false
    }
    
    @IBAction func skip(_ sender: Any) {
        
        if let nav = self.navigationController as? MainNavigationController {
            nav.showIntroVC(animated: true)
        }
    }
    
    @IBAction func showSignUpView(_ sender: Any) {
        
        if let nav = self.navigationController as? MainNavigationController {
            nav.showLoginVC()
        }
    }
}
