//
//  SearchViewController.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/9/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import GooglePlaces
import AlamofireImage
import Firebase

protocol SearchViewControllerDelegate:class {
    
    func searchView(_ searchView:SearchViewController, didSelectPlace place:WebPlace)
    func searchView(_ searchView:SearchViewController, didSelectResults results:[WebPlace])
    func searchViewWillDismiss(_ searchView:SearchViewController)
}

class SearchViewController: UIViewController, UITextFieldDelegate, RadiusViewControllerDelegate {
    
    class func presentOn<T:UIViewController>(_ viewController:T) where T:SearchViewControllerDelegate {
        
        if let searchVC = viewController.storyboard?.instantiateViewController(withIdentifier: "searchVC") as? SearchViewController {
            
            searchVC.delegate = viewController
            viewController.present(searchVC, animated: true, completion: nil)
        }
    }
    
    weak var delegate:SearchViewControllerDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var radiusButton: UIButton!
    
    @IBAction func showSideMenu(_ sender: Any) {
        
        self.delegate?.searchViewWillDismiss(self)
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NotificationNames.ShowSideMenu, object: nil)
        }
    }
    
    var results = [WebPlace]()
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var fieldContainer: UIView!
    
    @IBOutlet weak var resultsBorder: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.stopAnimating()
        
        self.noResultsLabel.isHidden = true
        self.resultsBorder.isHidden = true
        self.fieldContainer.layer.cornerRadius = 3
        self.fieldContainer.applyDarkShadow(opacity: 0.2, offset: CGSize(width: 0.5, height: 1), radius: 1.0)
        self.tableView.applyDarkShadow(opacity: 0.2, offset: CGSize(width: 0.5, height: 1), radius: 1.0)
        self.radiusButton.isExclusiveTouch = true
    }
    
    @IBAction func dismissViewOnTapping(_ sender: Any) {
        
        self.view.endEditing(true)
        self.delegate?.searchViewWillDismiss(self)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.textField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.textField.becomeFirstResponder()
        
        if self.results.count > 0 {
            
            self.delegate?.searchView(self, didSelectResults: self.results)
            self.delegate?.searchViewWillDismiss(self)
            self.dismiss(animated: true, completion: nil)
        }
        
        return true
    }
    
    var searchText:String?
    var timer:Timer?
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        self.searchText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector( checkTextFieldForContent ), userInfo: nil, repeats: false)
        return true
    }
    
    func checkTextFieldForContent() -> Void {
        
        let phrase = (self.searchText ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        
        if phrase.isEmpty {
            
            self.results.removeAll()
            self.tableView.reloadData()
            self.resultsBorder.isHidden = true
            self.noResultsLabel.isHidden = true
            
        } else {
            
            self.requestPlacesSearch(phrase)
        }
        
        self.timer = nil
    }
    
    @IBAction func openRadiusController(_ sender: Any) {
        
        RadiusViewController.presentOn(self, withDelegate: self)
    }
    
    func radiusController(_ radiusVC: RadiusViewController, didSelectRadius radius: Double) {
        
        self.checkTextFieldForContent()
    }
    
    func requestPlacesSearch(_ phrase:String) -> Void {
        
        let location = LocationService.shared.certainLocation
        self.activityIndicator.startAnimating()
        
        Analytics.logEvent(AnalyticsEventSearch, parameters: [
            AnalyticsParameterSearchTerm: phrase
        ])
        
        self.results.removeAll()
        self.noResultsLabel.isHidden = true
        
        let group = DispatchGroup()
        
        group.enter()
        AppWebApi.searchPlaces(location: location, keyword: phrase, completion: { (loadedPlaces) in
            
            if let places = loadedPlaces {
                print("SERVER RESULTS: \(places.count)")
                self.results.insert(contentsOf: places, at: 0)
            }
            
            group.leave()
        })
        
        group.enter()
        PlacesWebAPI.getPlacesNearby(location, keyword: phrase, completion: { (gplaces) in
            
            if let places = gplaces {
                
                print("GOOGLE RESULTS: \(places.count)")
                self.results.append(contentsOf: places)
            }
            
            group.leave()
        })
        
        group.notify(queue: .main) {
            
            print("Server Places with Google Id: \( self.results.filter({ $0.serverGooglePlaceId != nil }).count )")
            print("Total Result Count: \( self.results.count )")
            
            var cleanResults = [WebPlace]()
            var uniqueSet = Set<String>()
            for place in self.results {
                
                let plcId = place.serverGooglePlaceId ?? place.placeID
                if uniqueSet.contains(plcId) == false {
                    cleanResults.append(place)
                    uniqueSet.insert(plcId)
                }
            }
            
            print("Duplicate Results: \( self.results.count - cleanResults.count )")
            print("---")
            
            self.results = cleanResults
            self.activityIndicator.stopAnimating()
            self.tableView.reloadData()
            self.resultsBorder.isHidden = self.results.isEmpty
            self.noResultsLabel.isHidden = self.results.isEmpty == false
        }
    }
    
    @IBOutlet weak var noResultsLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableBelowConst: NSLayoutConstraint!
    func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            self.tableBelowConst.constant = keyboardFrame.cgRectValue.height
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    let dillunaIcon = #imageLiteral(resourceName: "dilluna_icon").af_imageAspectScaled(toFit: CGSize(width: 25, height: 25)).af_imageRounded(withCornerRadius: 5)
    
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchResultCell", for: indexPath)
        let place = self.results[indexPath.row]
        cell.textLabel?.text = place.name
        cell.detailTextLabel?.text = place.vicinity
        cell.imageView?.image = place.source == .server ? dillunaIcon : nil
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = self.results[indexPath.row]
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: place.name,
            AnalyticsParameterContentType: "search-result-googleplaces"
        ])
        
        self.delegate?.searchView(self, didSelectPlace: place)
        self.delegate?.searchViewWillDismiss(self)
        self.dismiss(animated: true, completion: nil)
    }
}
