//
//  MainNavigationController.swift
//  BasicAppTemplate-iOS
//
//  Created by Ali Hajjaj on 3/18/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import SideMenu
import SVProgressHUD
import GooglePlaces

final class LaunchCondition {
    
    static let current = LaunchCondition()
    
    let userDefaults: UserDefaults = .standard
    
    private var wasLaunchedBefore: Bool
    var isFirstLaunch: Bool {
        return !wasLaunchedBefore
    }
    
    private init() {
        let key = "com.imagine.Dilluna.FirstLaunch.WasLaunchedBefore"
        let wasLaunchedBefore = userDefaults.bool(forKey: key)
        self.wasLaunchedBefore = wasLaunchedBefore
        if !wasLaunchedBefore {
            userDefaults.set(true, forKey: key)
        }
    }
}

class BaseViewController:UIViewController {
    
    var isSidMenuSwipeEnabled:Bool = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let navVC = self.navigationController as? MainNavigationController {
            navVC.isSideMenuSwipeEnabled = self.isSidMenuSwipeEnabled
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let navVC = self.navigationController as? MainNavigationController {
            navVC.isSideMenuSwipeEnabled = true
        }
    }
}

class MainNavigationController: UINavigationController {
    
    var screenEdgeRecognizers:[UIGestureRecognizer]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let menuVC = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuVC") as? SideMenuViewController {
            
            menuVC.targetNavigation = self
            
            let menuNavVC = UISideMenuNavigationController(rootViewController: menuVC)
            menuNavVC.isNavigationBarHidden = true
            
            SideMenuManager.default.menuWidth = self.view.frame.width * 0.8
            SideMenuManager.default.menuAnimationFadeStrength = 0.6
            SideMenuManager.default.menuPresentMode = .menuSlideIn
            SideMenuManager.default.menuFadeStatusBar = false
            
            if Locale.current.identifier.hasPrefix("en") {
                
                SideMenuManager.default.menuLeftNavigationController = menuNavVC
                SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationBar)
                self.screenEdgeRecognizers = SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.view, forMenu: UIRectEdge.left)
            
            } else {
                
                SideMenuManager.default.menuRightNavigationController = menuNavVC
                SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationBar)
                self.screenEdgeRecognizers = SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.view, forMenu: UIRectEdge.right)
            }
        }
        
        NotificationCenter.default.addObserver(forName: NotificationNames.ShowSideMenu, object: nil, queue: nil) { (notif) in
            
            let menuVC = UserDefaults.languageCode!.hasPrefix("en")
                ? SideMenuManager.default.menuLeftNavigationController!
                : SideMenuManager.default.menuRightNavigationController!
            
            self.present(menuVC, animated: true, completion: {_ in})
        }
        
        NotificationCenter.default.addObserver(forName: NotificationNames.AppLaunchUsingAdCategoryNotification, object: nil, queue: nil) { (notif) in
            
            let catId = notif.userInfo!["ads_category"] as! String
            let appLaunch = notif.userInfo!["app_launch"] as! Bool
            
            self.loadAdsDetails(catId, appLaunch: appLaunch)
        }
    }
    
    private var _isSideMenuSwipeEnabled:Bool = true
    var isSideMenuSwipeEnabled:Bool {
        
        get { return self._isSideMenuSwipeEnabled  }
        set {
            self._isSideMenuSwipeEnabled = newValue
            self.screenEdgeRecognizers?.forEach { (reco) in
                reco.isEnabled = newValue
            }
        }
    }
    
    private var pendingIntroVCs:[UIViewController]?
    
    func loadAdsDetails(_ categoryId:String, appLaunch:Bool) -> Void {
        
        let memberId = AppUserInfo.current.userId
        let fcmToken = UserDefaults.standard.string(forKey: "fcmToken")
        
        // Trying to load category info
        
        SVProgressHUD.show()
        AppWebApi.fetchAdsCategories(forMember: memberId, andUserFCMToken: fcmToken, onSuccess: { (loadedCats) in
            
            SVProgressHUD.dismiss()
            
            let category:AdCategory
            if let cat = loadedCats?.first(where: { $0.id == categoryId }) {
                category = cat
            } else {
                category = AdCategory(id: categoryId, name: "dilluna-ads".local, image: "")
                category.image = #imageLiteral(resourceName: "ads_dummy")
            }
            
            self.showAdsDetails(forCategory: category, appLaunch: appLaunch)
            
        }) { (error) in
            
            SVProgressHUD.dismiss()
            print(error)
            
            let category = AdCategory(id: categoryId, name: "dilluna-ads".local, image: "")
            category.image = #imageLiteral(resourceName: "ads_dummy")
            
            self.showAdsDetails(forCategory: category, appLaunch: appLaunch)
        }
    }
    
    func showAdsDetails(forCategory category:AdCategory, appLaunch:Bool) -> Void {
        
        DispatchQueue.main.async {
            
            if let adsVC = self.storyboard?.instantiateViewController(withIdentifier: "adsVC"),
                let adsDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "adsDetailsVC") as? AdsDetailsViewController {
                
                adsDetailsVC.currentCategory = category
                self.pendingIntroVCs = nil
                
                var vcs = self.viewControllers
                let pushVCs = {
                    if vcs.count > 1 {
                        vcs.removeLast(vcs.count - 1)
                    }
                    
                    vcs.append(contentsOf: [adsVC, adsDetailsVC])
                    self.setViewControllers(vcs, animated: true)
                }
                
                if appLaunch {
                    
                    if let firstVC = vcs.first, firstVC is IntroViewController {
                        pushVCs()
                    } else {
                        self.pendingIntroVCs = [adsVC, adsDetailsVC]
                    }
                    
                } else {
                    
                    pushVCs()
                }
            }
        }
    }
    
    func showIntroVC(animated:Bool) -> Void {
        
        print("show intro")
        if let introVC = self.storyboard?.instantiateViewController(withIdentifier: "introVC") {
            self.isSideMenuSwipeEnabled = true
            
            var vcs = [introVC]
            if let pendVCs = self.pendingIntroVCs {
                
                vcs.append(contentsOf: pendVCs)
                self.pendingIntroVCs = nil
            }
            
            self.setViewControllers(vcs, animated: animated)
        }
    }
    
    func showFirstLaunchVC() -> Void {
        
        if let firstVC = self.storyboard?.instantiateViewController(withIdentifier: "firstVC") {
            self.pushViewController(firstVC, animated: true)
        }
    }
    
    func showConfirmVC() -> Void {
        
        if let confirmVC = self.storyboard?.instantiateViewController(withIdentifier: "confirmVC") as? ConfirmSignUpViewController {
            self.pushViewController(confirmVC, animated: true)
        }
    }
    
    func showLoginVC() -> Void {
        
        if let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") {
            self.pushViewController(loginVC, animated: true)
        }
    }
    
    func showSignUpVC() -> Void {
        
        if let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "signUpVC") {
            self.pushViewController(signUpVC, animated: true)
        }
    }
    
    func showDayProgress() -> Void {
        
        if let todayVC = self.storyboard?.instantiateViewController(withIdentifier: "todayVC") {
            self.pushViewController(todayVC, animated: true)
        }
    }
    
    func showPlaceDetails(_ place:PlaceDisp, focusOnOffers:Bool = false) -> Void {
        
        let popUpDetails = {
            
            if place.data.source == .google {
                if let placeDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "placeDetailsVC_google") as? GooglePlaceDetailsViewController {
                    placeDetailsVC.currentPlace = place
                    self.pushViewController(placeDetailsVC, animated: true)
                }
            } else {
                
                if let placeDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "placeDetailsVC") as? PlaceDetailsViewController {
                    placeDetailsVC.currentPlace = place
                    placeDetailsVC.focusOnOffers = focusOnOffers
                    self.pushViewController(placeDetailsVC, animated: true)
                }
            }
        }
        
        place.photos.removeAll()
        
        if place.data.source == .google {
            
            place.details = nil
            
            SVProgressHUD.show()
            
            var reqError:Error?
            let group = DispatchGroup()
            
            group.enter()
            GMSPlacesClient.shared().lookUpPlaceID(place.id, callback: { (details, error) in
                place.details = details
                reqError = error
                group.leave()
            })
            
            group.enter()
            GMSPlacesClient.shared().lookUpPhotos(forPlaceID: place.id) { (photos, error) in
                
                group.leave()
                
                if let _photosMeta = photos?.results {
                    
                    place.photos.append(contentsOf: _photosMeta.map({ (meta) -> PlacePhotoData in
                        return PlacePhotoData(meta: meta)
                    }))
                }
            }
            
            group.notify(queue: .main, execute: {
                
                SVProgressHUD.dismiss()
                
                if place.details != nil {
                    popUpDetails()
                } else {
                    showErrorMessage( reqError?.localizedDescription ?? "random-error".local )
                }
            })
            
        } else {
            
            place.info = nil
            AppWebApi.getPlaceDetails(place.data) { (info, photos) in
                
                place.info = info
                place.photos.append(contentsOf: (photos ?? []).map({ (path) -> PlacePhotoData in
                    return PlacePhotoData(url: path)
                }) )
                
                if let googlePlaceId = place.data.serverGooglePlaceId {
                    
                    GMSPlacesClient.shared().lookUpPhotos(forPlaceID: googlePlaceId) { (photos, error) in
                        
                        if let _photosMeta = photos?.results {
                            place.photos.append(contentsOf: _photosMeta.map({ (meta) -> PlacePhotoData in
                                return PlacePhotoData(meta: meta)
                            }) )
                        }
                        
                        popUpDetails()
                    }
                    
                } else {
                    
                    popUpDetails()
                }
            }
        }
    }
    
}
