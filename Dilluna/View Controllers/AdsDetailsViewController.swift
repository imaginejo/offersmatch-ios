//
//  AdsListViewController.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/22/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit

class AdsDetailsViewController: UIViewController {
    
    var currentCategory:AdCategory!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    
    var adItems = [AdItemViewModel]()
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let img = self.currentCategory.image {
            self.categoryImageView.image = img
        } else if let imgUrl = URL(string: self.currentCategory.imageUrl) {
            self.categoryImageView.af_setImage(withURL: imgUrl)
        }
        
        self.categoryLabel.text = self.currentCategory.name
        self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector( fetchAdsItems ), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = self.refreshControl
        } else {
            self.tableView.addSubview(self.refreshControl)
        }
        
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIDeviceOrientationDidChange, object: nil, queue: nil)
        { (notif) in
            
            self.tableView.reloadData()
        }
        
        self.fetchAdsItems()
    }
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var loadingError:String?
    func fetchAdsItems() -> Void {
        
        self.loadingError = nil
        
        if !self.refreshControl.isRefreshing {
            self.indicator.startAnimating()
        }
        
        let successHandler:AppWebApi.AdsItemsSuccessHandler = { (loadedItems) in
            
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            } else {
                self.indicator.stopAnimating()
            }
            
            if let items = loadedItems {
                
                self.adItems = items.map({ (item) -> AdItemViewModel in
                    return AdItemViewModel(item)
                })
                
                self.tableView.reloadData()
            }
        }
        
        let failureHandler:AppWebApi.FailureHandler = { (error) in
            
            self.loadingError = error
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            } else {
                self.indicator.stopAnimating()
            }
            
            print(error)
        }
        
        if let userId = AppUserInfo.current.userId {
            
            AppWebApi.fetchAdsItems(forCategory: self.currentCategory.id,
                                    andMember: userId,
                                    onSuccess: successHandler,
                                    onFailure: failureHandler)
            
        } else if let token = UserDefaults.standard.string(forKey: "fcmToken") {
            
            AppWebApi.fetchAdsItems(forCategory: self.currentCategory.id,
                                    andVisitor: token,
                                    onSuccess: successHandler,
                                    onFailure: failureHandler)
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        self.currentCategory = nil
        self.adItems.forEach { (item) in item.image = nil }
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    deinit {
        self.currentCategory = nil
        print("delete Ads Details")
    }
}

extension AdsDetailsViewController: UITableViewDelegate, UITableViewDataSource, AdItemViewCellDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.adItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.adItems[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.adItems[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "adItemCell", for: indexPath) as! AdItemViewCell
        
        cell.delegate = self
        cell.showItem(self.adItems[indexPath.row])
        
        return cell
    }
    
    func adItemCellDidUpdateContent(_ cell: AdItemViewCell) {
        
        if let index = self.tableView.indexPath(for: cell) {
            self.tableView.reloadRows(at: [index], with: .automatic)
        }
    }
}


class AdItem {
    
    var text:String?
    var imageUrl:String?
    var date:String?
    
    init(text:String?, photo:String?, date:String?) {
        self.text = text
        self.imageUrl = photo
        self.date = date
    }
}

class AdItemViewModel {
    
    var item:AdItem
    var height:CGFloat = 0
    var image:UIImage?
    
    init(_ item:AdItem) {
        self.item = item
    }
}
