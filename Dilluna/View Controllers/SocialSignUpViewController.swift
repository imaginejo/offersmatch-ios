//
//  SocialSignUpViewController.swift
//  Dilluna
//
//  Created by Suhayb Ahmad on 5/21/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import Validator
import Alamofire
import AlamofireImage
import CoreLocation
import Firebase
import TrueTime

class SocialSignUpViewController: BaseViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var dialCodeView: DialCodeView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var profilePhotoView: UIImageView!
    @IBOutlet var roundViews: [UIView]!
    
    var socialInfo:SocialLoginInfo!
    var selectedImage:UIImage?
    var selectedGender:Int = 1  // 1: Male, 0: Female
    
    @IBOutlet var genderButtons: [UIButton]!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    
    let lightGrayColor = UIColor(white: 0.80, alpha: 1)
    let darkColor = UIColor(valueRed: 112, green: 144, blue: 171, alpha: 1)
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        }
        
        self.isSidMenuSwipeEnabled = false
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillShow, object: nil, queue: nil) { (notif) in
            
            if let frame = notif.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
                self.bottomConst.constant = frame.height
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillHide, object: nil, queue: nil) { _ in
            
            self.bottomConst.constant = 0
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        self.roundViews.forEach { (view) in
            view.layer.cornerRadius = 3
            view.applyDarkShadow(opacity: 0.3, offsetY: 1)
        }
        
        self.genderButtons.forEach { (btn) in
            btn.layer.cornerRadius = 3
            btn.applyDarkShadow(opacity: 0.3, offsetY: 1)
        }
        
        self.styleGenderButtons(forSelection: self.selectedGender)
        self.birthdateButton.setTitle("Select Birth Date".local, for: .normal)
        self.locationButton.setTitle("Select Location".local, for: .normal)
        
        self.birthdateButton.setTitleColor(lightGrayColor, for: .normal)
        self.locationButton.setTitleColor(lightGrayColor, for: .normal)
        self.nameField.text = self.socialInfo.name
        
        self.doSetSelectedImage(self.selectedImage)
    }
    
    @IBAction func cancelSignUp(_ sender: Any) {
        
        SocialLogin.logout(socialInfo.socialNetwork)
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func viewDidTap(_ sender: Any) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func genderSelected(_ sender: Any) {
        
        let button = sender as! UIButton
        self.selectedGender = button.tag
        self.styleGenderButtons(forSelection: button.tag)
    }
    
    func styleGenderButtons(forSelection tagSelected:Int) -> Void {
        
        self.genderButtons.forEach { (btn) in
            
            if btn.tag == tagSelected {
                
                btn.backgroundColor = darkColor
                btn.tintColor = UIColor.white
                
            } else {
                
                btn.backgroundColor = UIColor.white
                btn.tintColor = darkColor
            }
        }
    }
    
    @IBOutlet weak var birthdateButton: UIButton!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var locationButton: UIButton!
    
    var selectedBirthDate:Date?
    var selectedLocation:CLLocationCoordinate2D?
    var selectedAddress:String?
    
    @IBAction func launchBirthdateSelection(_ sender: Any) {
        
        DatePickerViewController.launch(delegate: self, initialDate: self.selectedBirthDate)
    }
    
    @IBAction func launchLocationSelection(_ sender: Any) {
        
        LocationSelectViewController.launch(delegate: self)
    }
    
    func doSetSelectedImage(_ image:UIImage? ) -> Void {
        
        self.selectedImage = image
        self.profilePhotoView.image = image
    }
    
    @IBAction func startPhotoPicking(_ sender: Any) {
        
        var imageRemovalAction:ImageRemovalBlock?
        if self.selectedImage != nil {
            
            imageRemovalAction = {
                self.doSetSelectedImage(nil)
            }
        }
        
        launchImageSelection(onView: self, sourceView: sender as! UIView, onImageRemoval: imageRemovalAction)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            let size = CGSize(width: 250, height: 250)
            let photo = image.af_imageAspectScaled(toFill: size)
            self.doSetSelectedImage(photo)
        }
        
        picker.dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitSignup(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let reqRule = ValidationRuleRequired<String>(error: ValidationError.name)
        guard let name = self.nameField.text, name.validate(rule: reqRule).isValid,
            let phone = self.phoneField.text, phone.validate(rule: reqRule).isValid,
            let birthdate = self.selectedBirthDate,
            let location = self.selectedLocation else {
                
                showErrorMessage("signup-error".local)
                return
        }
        
        let fullPhone = "\(self.dialCodeView.value)\(phone)"
        AppWebApi.socialSignUp(name: name,
                               email: self.socialInfo.email,
                               socialId: self.socialInfo.socialId,
                               socialNetwork: self.socialInfo.socialNetwork.rawValue,
                               birthdate: birthdate,
                               phone: fullPhone,
                               countryCode: self.dialCodeView.countryCode,
                               location: location,
                               address: self.selectedAddress,
                               gender: self.selectedGender,
                               photo: self.selectedImage,
                               success: { (userId, photoUrl, date) in
            
            AppUserInfo.current.userId = userId
            AppUserInfo.current.name = name
            AppUserInfo.current.email = self.socialInfo.email
            AppUserInfo.current.photo = photoUrl
            AppUserInfo.current.registrationDate = date
            AppUserInfo.current.isConfirmed = true
            
            let ageComps = Calendar.current.dateComponents([.year], from: birthdate, to: Date())
            Analytics.setUserProperty(self.selectedGender == 1 ? "Male" : "Female", forName: "Gender")
            Analytics.setUserProperty("\(ageComps.year!)", forName: "Age")
            
            let signUpMethod = self.socialInfo.socialNetwork == .Google ? "Google" : "Facebook"
            Analytics.logEvent(AnalyticsEventSignUp, parameters: [
                AnalyticsParameterSignUpMethod: signUpMethod
            ])
            
            VisitsLog.shared.recreate()
            /*if let today = TrueTimeClient.sharedInstance.referenceTime?.now() {
                
                VisitsLog.shared.markVisited(today)
                if let thirtyDaysVC = self.storyboard?.instantiateViewController(withIdentifier: "thirtyDaysVC") as? ThirtyDaysProgressViewController {
                    self.navigationController?.pushViewController(thirtyDaysVC, animated: true)
                }
                
            } else {*/
                
                self.navigationController?.dismiss(animated: true, completion: nil)
            //}
            
        }, failure: { (error) in
            
            showErrorMessage(error)
        })
    }
    
}

extension SocialSignUpViewController: DatePickerViewControllerDelegate, LocationSelectViewControllerDelegate {
    
    func datePickerControllerDidSelectDate(_ selectedDate: Date) {
        
        self.selectedBirthDate = selectedDate
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        
        self.birthdateButton.setTitle(dateFormatter.string(from: selectedDate), for: .normal)
        self.birthdateButton.setTitleColor(UIColor.black, for: .normal)
    }
    
    func datePickerControllerDidClearSelection(_ datePickerVC: DatePickerViewController) {
        
        self.selectedBirthDate = nil
        self.birthdateButton.setTitle("Select Birth Date".local, for: .normal)
        self.birthdateButton.setTitleColor(lightGrayColor, for: .normal)
    }
    
    func locationSelectDidReceiveAddress(_ address: String, atCoordinates coordinates: CLLocationCoordinate2D) {
        
        self.selectedLocation = coordinates
        self.selectedAddress = address
        
        self.dialCodeView.setLocation(coordinates)
        self.locationButton.setTitle(address, for: .normal)
        self.locationButton.setTitleColor(UIColor.black, for: .normal)
    }
}

