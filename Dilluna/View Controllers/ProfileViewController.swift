//
//  ProfileViewController.swift
//  Dilluna
//
//  Created by Suhayb Ahmad on 5/6/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import Validator
import Alamofire
import AlamofireImage
import CoreLocation
import Firebase
import SVProgressHUD

class ProfileViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var dialCodeView: DialCodeView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var profilePhotoView: UIImageView!
    
    @IBOutlet var roundViews: [UIView]!
    @IBOutlet weak var birthdateButton: UIButton!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet var genderButtons: [UIButton]!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var selectedBirthDate:Date?
    var selectedLocation:CLLocationCoordinate2D?
    var selectedAddress:String?
    var selectedImage:UIImage?
    var selectedGender:Int = 1  // 1: Male, 2: Female
    
    weak var currentProfile:UserProfile!
    
    let lightGrayColor = UIColor(white: 0.80, alpha: 1)
    let darkColor = UIColor(valueRed: 112, green: 144, blue: 171, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        }
        
        self.doSetSelectedImage(nil)
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillShow, object: nil, queue: nil) { (notif) in
            
            if let frame = notif.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
                self.bottomConst.constant = frame.height
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillHide, object: nil, queue: nil) { _ in
            
            self.bottomConst.constant = 0
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        self.roundViews.forEach { (view) in
            view.layer.cornerRadius = 3
            view.applyDarkShadow(opacity: 0.3, offsetY: 1)
        }
        
        self.genderButtons.forEach { (btn) in
            btn.layer.cornerRadius = 3
            btn.applyDarkShadow(opacity: 0.3, offsetY: 1)
        }
        
        self.updateFields()
    }
    
    var isOriginalImageWasSet:Bool = false
    var didImageChange:Bool = false
    func updateFields() -> Void {
        
        let profile = self.currentProfile!
        
        self.nameField.text = profile.name
        self.setLocation(profile.coords, andAddress: profile.address) { (value) in
            self.phoneField.text = profile.phone.replacingOccurrences(of: value, with: "")
        }
        
        self.selectGender(profile.gender)
        self.setBirthDate( profile.birthdate)
        
        if let photoPath = profile.photoUrl, let url = URL(string: photoPath) {
            
            Alamofire.request(url).responseImage { (response) in
                self.doSetSelectedImage(response.result.value)
                self.isOriginalImageWasSet = true
            }
        }
    }
    
    @IBAction func viewDidTap(_ sender: Any) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func genderSelected(_ sender: Any) {
        
        let button = sender as! UIButton
        self.selectGender(button.tag)
    }
    
    func selectGender(_ gender:Int) -> Void {
        
        self.selectedGender = gender
        self.genderButtons.forEach { (btn) in
            
            if btn.tag == gender {
                
                btn.backgroundColor = darkColor
                btn.tintColor = UIColor.white
                
            } else {
                
                btn.backgroundColor = UIColor.white
                btn.tintColor = darkColor
            }
        }
    }
    
    @IBAction func launchBirthdateSelection(_ sender: Any) {
        
        DatePickerViewController.launch(delegate: self, initialDate: self.selectedBirthDate)
    }
    
    @IBAction func launchLocationSelection(_ sender: Any) {
        
        LocationSelectViewController.launch(delegate: self)
    }
    
    func doSetSelectedImage(_ image:UIImage? ) -> Void {
        
        self.selectedImage = image
        self.profilePhotoView.image = image
    }
    
    @IBAction func startPhotoPicking(_ sender: Any) {
        
        var imageRemovalAction:ImageRemovalBlock?
        if self.selectedImage != nil {
            
            imageRemovalAction = {
                self.doSetSelectedImage(nil)
            }
        }
        
        launchImageSelection(onView: self, sourceView: sender as! UIView, onImageRemoval: imageRemovalAction)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            let size = CGSize(width: 250, height: 250)
            let photo = image.af_imageAspectScaled(toFill: size)
            self.doSetSelectedImage(photo)
            self.didImageChange = true
        }
        
        picker.dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitProfileUpdate(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let reqRule = ValidationRuleRequired<String>(error: ValidationError.name)
        
        guard let name = self.nameField.text, name.validate(rule: reqRule).isValid,
            let phone = self.phoneField.text, phone.validate(rule: reqRule).isValid,
            let birthdate = self.selectedBirthDate,
            let location = self.selectedLocation else {
                
                showErrorMessage("signup-error".local)
                return
        }
        
        let fullPhone = "\(self.dialCodeView.value)\(phone)"
        
        let doRemovePhoto = self.selectedImage == nil && self.isOriginalImageWasSet
        let image = self.didImageChange ? self.selectedImage : nil
        
        print("image: \(String(describing: image))")
        SVProgressHUD.setMaximumDismissTimeInterval(1)
        
        AppWebApi.updateUserProfile(userId: AppUserInfo.current.userId!,
                                    name: name,
                                    birthdate: birthdate,
                                    phone: fullPhone,
                                    location: location,
                                    address: self.selectedAddress,
                                    gender: self.selectedGender,
                                    photo: image,
                                    removePhoto: doRemovePhoto,
                                    success:
        { (photoUrl) in
            
            AppUserInfo.current.photo = photoUrl
            SVProgressHUD.showSuccess(withStatus: "update-success".local )
            
        }, failure:{ (error) in
            
            SVProgressHUD.showSuccess(withStatus: "update-fail".local )
            print("Error updating profile: \(error)")
        })
    }
    
    @IBAction func closeView(_ sender: Any) {
        
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func logoutUser(_ sender: Any) {
        
        AppUserInfo.current.logout()
        
        if let regNavVC = self.navigationController as? RegistrationNavigationController {
            
            regNavVC.showLogin(animated: true)
        }
    }
}

extension ProfileViewController: DatePickerViewControllerDelegate, LocationSelectViewControllerDelegate {
    
    func datePickerControllerDidSelectDate(_ selectedDate: Date) {
        
        self.setBirthDate(selectedDate)
    }
    
    func setBirthDate(_ date:Date) -> Void {
        
        self.selectedBirthDate = date
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        
        self.birthdateButton.setTitle(dateFormatter.string(from: date), for: .normal)
        self.birthdateButton.setTitleColor(UIColor.black, for: .normal)
    }
    
    func clearBirthDate() -> Void {
        
        self.birthdateButton.setTitle("Select Birth Date".local, for: .normal)
        self.birthdateButton.setTitleColor(lightGrayColor, for: .normal)
    }
    
    func datePickerControllerDidClearSelection(_ datePickerVC: DatePickerViewController) {
        
        self.clearBirthDate()
    }
    
    func locationSelectDidReceiveAddress(_ address: String, atCoordinates coordinates: CLLocationCoordinate2D) {
        
        self.setLocation(coordinates, andAddress: address)
    }
    
    func setLocation(_ location:CLLocationCoordinate2D, andAddress address:String?, completion:((String) -> Void)? = nil ) -> Void {
        
        self.selectedLocation = location
        self.selectedAddress = address
        
        self.dialCodeView.setLocation(location, completion: completion)
        self.locationButton.setTitle(address, for: .normal)
        self.locationButton.setTitleColor(UIColor.black, for: .normal)
    }
}
