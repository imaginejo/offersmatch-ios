//
//  IntroViewController.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/9/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController, SearchViewControllerDelegate {
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchView.layer.cornerRadius = 3
        self.searchView.applyDarkShadow(opacity: 0.2, offset: CGSize(width: 0.5, height: 1), radius: 1.0)
    }
    
    @IBAction func showSideMenu(_ sender: Any) {
        
        NotificationCenter.default.post(name: NotificationNames.ShowSideMenu, object: nil)
    }
    
    @IBAction func openSearchView(_ sender: Any) {
        
        self.container.isHidden = true
        self.menuButton.isHidden = true
        SearchViewController.presentOn(self)
    }
    
    @IBAction func openRadiusController(_ sender: Any) {
        
        if let topVC = UIApplication.topViewController() {
            RadiusViewController.presentOn(topVC, withDelegate: nil)
        }
    }
    
    var selectedPlace:WebPlace!
    func searchView(_ searchView: SearchViewController, didSelectPlace place: WebPlace) {
        
        self.selectedPlace = place
        self.performSegue(withIdentifier: "intro_to_map", sender: nil)
    }
    
    var selectedResults:[WebPlace]!
    func searchView(_ searchView: SearchViewController, didSelectResults results: [WebPlace]) {
        
        self.selectedResults = results
        self.performSegue(withIdentifier: "intro_to_map", sender: nil)
    }
    
    func searchViewWillDismiss(_ searchView: SearchViewController) {
        
        self.container.isHidden = false
        self.menuButton.isHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination is HomeViewController {
            (segue.destination as! HomeViewController).initialResults = self.selectedResults
            (segue.destination as! HomeViewController).initialPlace = self.selectedPlace
        }
    }
}

