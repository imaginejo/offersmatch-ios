//
//  RadiusViewController.swift
//  Dilluna
//
//  Created by Suhayb Ahmad on 7/15/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

protocol RadiusViewControllerDelegate:class {
    func radiusController(_ radiusVC:RadiusViewController, didSelectRadius radius:Double) -> Void
}

class RadiusViewController: UIViewController, GMSMapViewDelegate, LocationServiceDelegate {
    
    class func presentOn<Type: UIViewController & RadiusViewControllerDelegate>(_ vc:Type) {
        
        if let radiusVC = vc.storyboard?.instantiateViewController(withIdentifier: "radiusVC") as? RadiusViewController {
            
            radiusVC.delegate = vc
            vc.present(radiusVC, animated: true, completion: nil)
        }
    }
    
    class func presentOn(_ vc:UIViewController, withDelegate del:RadiusViewControllerDelegate?) {
        
        if let radiusVC = vc.storyboard?.instantiateViewController(withIdentifier: "radiusVC") as? RadiusViewController {
            
            radiusVC.delegate = del
            vc.present(radiusVC, animated: true, completion: nil)
        }
    }
    
    
    weak var delegate:RadiusViewControllerDelegate?
    
    var centerMark:GMSMarker!
    var circle:GMSCircle!
    
    var currentRadius:CLLocationDistance = AppUserInfo.current.searchRadius
    
    @IBOutlet weak var radiusLabel: UILabel!
    
    @IBAction func cancelPressed(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okayPressed(_ sender: Any) {
        
        self.dismiss(animated: true) {
            
            AppUserInfo.current.searchRadius = self.currentRadius
            self.delegate?.radiusController(self, didSelectRadius: self.currentRadius)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        LocationService.shared.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        LocationService.shared.delegate = nil
    }
    
    @IBAction func showMyLocation(_ sender: Any) {
        
        LocationService.shared.updateLocation()
        self.mapView.animate(toLocation: LocationService.shared.certainLocation)
    }
    
    func locationService(_ service: LocationService, didReceiveLocation location: CLLocationCoordinate2D) {
        
        self.mapView.animate(toLocation: LocationService.shared.certainLocation)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let circleCenter = LocationService.shared.certainLocation
        
        let path = GMSMutablePath()
        path.add( calculateLocation(farFrom: circleCenter, withDistance: self.currentRadius * 3 / 2, andBearing: 90) )
        path.add( calculateLocation(farFrom: circleCenter, withDistance: self.currentRadius * 3 / 2, andBearing: 270) )
        
        if let pos = self.mapView.camera(for: GMSCoordinateBounds(path: path), insets: UIEdgeInsets.zero) {
            
            self.mapView.animate(to: pos)
        }
        
        self.displayDistance()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        let loc1 = self.mapView.projection.visibleRegion().nearLeft
        let loc2 = self.mapView.projection.visibleRegion().nearRight
        
        let distance1 = CLLocation(latitude: loc1.latitude, longitude: loc1.longitude).distance(from: CLLocation(latitude: loc2.latitude, longitude: loc2.longitude) )
        
        let loc3 = self.mapView.projection.visibleRegion().farLeft
        let loc4 = self.mapView.projection.visibleRegion().nearLeft
        
        let distance2 = CLLocation(latitude: loc3.latitude, longitude: loc3.longitude).distance(from: CLLocation(latitude: loc4.latitude, longitude: loc4.longitude) )
        
        let raduis = min(distance1, distance2) / 3.0
        self.currentRadius = raduis
        self.circle.radius = raduis
        self.displayDistance()
    }
    
    func displayDistance() -> Void {
        
        self.radiusLabel.text = formatRadius(self.currentRadius)
    }
    
    @IBOutlet weak var controlsView: UIView!
    private var mapView:GMSMapView!
    override func loadView() {
        super.loadView()
        
        let circleCenter = LocationService.shared.certainLocation
        let camera = GMSCameraPosition.camera(
            withLatitude: circleCenter.latitude,
            longitude: circleCenter.longitude,
            zoom: 16.0)
        
        
        self.mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        self.mapView.delegate = self
        self.mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
        self.view.insertSubview(self.mapView, at: 0)
        
        self.view.addConstraint(NSLayoutConstraint(item: self.mapView, attribute: .top, relatedBy: .equal,
                                                   toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
        
        self.view.addConstraint(NSLayoutConstraint(item: self.mapView, attribute: .left, relatedBy: .equal,
                                                   toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        
        self.view.addConstraint(NSLayoutConstraint(item: self.mapView, attribute: .right, relatedBy: .equal,
                                                   toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        
        self.view.addConstraint(NSLayoutConstraint(item: self.mapView, attribute: .bottom, relatedBy: .equal,
                                                   toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0))
        
        circle = GMSCircle(position: circleCenter, radius: self.currentRadius)
        circle.map = mapView
        circle.strokeColor = UIColor(red: 1, green: 0.3, blue: 0.3, alpha: 0)
        circle.strokeWidth = 1
        circle.fillColor = UIColor(red: 0.8, green: 0.1, blue: 0.1, alpha: 0.1)
        
        centerMark = GMSMarker(position: circleCenter)
        centerMark.icon = #imageLiteral(resourceName: "radius-mark-icon")
        centerMark.isDraggable = false
        centerMark.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        centerMark.map = self.mapView
    }
}

func formatRadius(_ value:Double ) -> String {
    
    let nf = NumberFormatter()
    nf.maximumFractionDigits = 2
    nf.minimumIntegerDigits = 1
    
    return nf.string(from: NSNumber(value:  value / 1000))! + " \( "km".local )"
}

func calculateLocation(farFrom loc:CLLocationCoordinate2D, withDistance distance:CLLocationDistance, andBearing bearingDegrees:Double) -> CLLocationCoordinate2D {

    let toRadian = Double.pi / 180
    
    let R = 6378100.0
    let bearing = bearingDegrees * toRadian
    
    let lat1 = loc.latitude * toRadian
    let lon1 = loc.longitude * toRadian
    
    let lat2 = asin( sin(lat1) * cos(distance / R) + cos(lat1) * sin(distance / R) * cos(bearing) )
    let lon2 = lon1 + atan2( sin(bearing) * sin(distance / R) * cos(lat1), cos(distance / R) - sin(lat1) * sin(lat2) )
    
    return CLLocationCoordinate2D(latitude: lat2 / toRadian, longitude: lon2 / toRadian)
}
