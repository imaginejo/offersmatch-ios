//
//  OffersListViewController.swift
//  Offers
//
//  Created by AhmeDroid on 4/19/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import CoreLocation

class OffersListViewController: UIViewController {
    
    @IBOutlet weak var searchBox: UIView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet var offersTabelView: UITableView!
    
    var placesList = [PlaceDisp]()
    var refreshControl:UIRefreshControl!
    var timer:Timer?
    
    var currentLocation:CLLocation?
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBox.layer.cornerRadius = 3
        self.searchBox.applyDarkShadow(opacity: 0.15, offset: CGSize(width: 0.2, height: 1.0 ) , radius : 2 )
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector( checkTextFieldForContent ), for: .valueChanged)
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        
        if #available(iOS 10.0, *) {
            self.offersTabelView.refreshControl = self.refreshControl
        } else {
            self.offersTabelView.addSubview(self.refreshControl)
        }
        
        if #available(iOS 11.0, *) {
            self.offersTabelView.contentInsetAdjustmentBehavior = .never
        }
        
        self.requestPlacesSearch()
    }
    
    @IBAction func cancelEditing(_ sender: Any) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func showSideMenu(_ sender: Any) {
        
        NotificationCenter.default.post(name: NotificationNames.ShowSideMenu, object: nil)
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        self.locationManager.delegate = nil
        self.locationManager.stopUpdatingLocation()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    var searchText:String?
}

extension OffersListViewController: UITextFieldDelegate, RadiusViewControllerDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        self.searchText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector( checkTextFieldForContent ), userInfo: nil, repeats: false)
        return true
    }
    
    func checkTextFieldForContent() -> Void {
        
        let phrase = (self.searchText ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        self.requestPlacesSearch(phrase)
        self.timer = nil
    }
    
    func requestPlacesSearch(_ phrase:String = "") -> Void {
        
        guard let location = self.currentLocation?.coordinate else {
            return
        }
        
        if self.refreshControl.isRefreshing == false {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        AppWebApi.fetchPlacesWithOffers(location: location, keyword: phrase) { (places) in
            
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
            var count:Int = 0
            
            places?.filter({ (rplace) -> Bool in
                return self.placesList.contains(where: { (disp) -> Bool in
                    return disp.data.placeID == rplace.placeID
                }) == false
            }).forEach({ (place) in
                
                count += 1
                self.placesList.insert(PlaceDisp(place), at: 0)
            })
            
            if count > 0 {
                
                var indices = [IndexPath]()
                for i in 0 ..< count {
                    indices.append(IndexPath(row: i, section: 0))
                }
                
                self.offersTabelView.tableHeaderView = nil
                self.offersTabelView.insertRows(at: indices, with: .automatic)
            }
        }
    }
    
    @IBAction func openRadiusController(_ sender: Any) {
        
        if let topVC = UIApplication.topViewController() {
            
            RadiusViewController.presentOn(topVC, withDelegate: self)
        }
    }
    
    func radiusController(_ radiusVC: RadiusViewController, didSelectRadius radius: Double) {
        
        self.checkTextFieldForContent()
    }
}

extension OffersListViewController: UITableViewDataSource, UITableViewDelegate, OffersTableViewCellDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.placesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Offers", for: indexPath ) as! OffersTableViewCell
        let place = self.placesList[indexPath.row]
        
        cell.delegate = self
        cell.showPlace(place)
        
        return cell
    }
    
    func offersPlaceCell(_ cell: OffersTableViewCell, didRequireInfoShowForPlace place: PlaceDisp) {
        
        if let navVC = self.navigationController as? MainNavigationController {
            navVC.showPlaceDetails(place, focusOnOffers: true)
        }
    }
}

extension OffersListViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            print("Start monitoring")
            self.locationManager.startUpdatingLocation()
        case .denied:
            showErrorMessage("This feature can not work properly without giving access to your location. To turn on near Offers discovery feature, please go to Settings and give access again to this app.")
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let loc = locations.last else {
            return
        }
        
        if let ploc = self.currentLocation, ploc.distance(from: loc) >= 50 {
            
            self.currentLocation = loc
            self.requestPlacesSearch()
            
        } else {
            
            self.currentLocation = loc
            self.requestPlacesSearch()
        }
    }
}
