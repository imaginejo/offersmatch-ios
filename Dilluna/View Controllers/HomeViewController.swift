//
//  HomeViewController.swift
//  OffersMatch
//
//  Created by Ali Hajjaj on 3/14/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import SVProgressHUD
import Firebase
import Alamofire
import UberRides

class HomeViewController: BaseViewController, SearchViewControllerDelegate {
    
    var initialPlace:WebPlace?
    var initialResults:[WebPlace]?
    
    @IBOutlet weak var searchView: UIView!
    @IBAction func showSideMenu(_ sender: Any) {
        
        NotificationCenter.default.post(name: NotificationNames.ShowSideMenu, object: nil)
    }
   
    @IBOutlet weak var searchButton: UIButton!
    @IBAction func openSearchAutoComplete(_ sender: Any) {
        
        SearchViewController.presentOn(self)
    }
    
    func searchView(_ searchView: SearchViewController, didSelectPlace place: WebPlace) {
        
        self.fetchNearbySimilarPlaces(toPlace: place)
    }
    
    func searchView(_ searchView: SearchViewController, didSelectResults results: [WebPlace]) {
        
        self.loadPlacesCompletionHandler(nil, results)
    }
    
    func searchViewWillDismiss(_ searchView: SearchViewController) {}
    
    var mapView:GMSMapView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let m = (self.view.frame.width - 220) / 2.0
        let snappingLayout = SnappingCollectionViewLayout()
        snappingLayout.itemSize = CGSize(width: 220, height: 220)
        snappingLayout.minimumLineSpacing = 15
        snappingLayout.minimumInteritemSpacing = 10000
        snappingLayout.sectionInset = UIEdgeInsets(top: 20, left: m, bottom: 20, right: m)
        snappingLayout.scrollDirection = .horizontal
        
        self.collectionView.collectionViewLayout = snappingLayout
        self.collectionView.decelerationRate = UIScrollViewDecelerationRateFast
        
        self.searchView.layer.cornerRadius = 3
        self.searchView.applyDarkShadow(opacity: 0.20, offset: CGSize(width: 0.5, height: 1), radius: 1)
        
        let location = LocationService.shared.defaultLocation
        let camera = GMSCameraPosition.camera(withLatitude: location.latitude, longitude: location.longitude, zoom: 17.0)
        let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        
        if let styleUrl = Bundle.main.url(forResource: "map_style", withExtension: "json") {
            do {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleUrl)
            } catch { print(error.localizedDescription) }
        }
        
        self.view.insertSubview(mapView, at: 0)
        self.mapView = mapView
        self.mapView.delegate = self
        
        
        if let results = self.initialResults {
            
            self.loadPlacesCompletionHandler(nil, results)
        } else if let place = self.initialPlace {
            
            self.fetchNearbySimilarPlaces(toPlace: place)
        }
    }
    
    let sampleRestIcon = UIImage(named: "costa-coffee")
    var places = [PlaceDisp]()
    
    func fetchNearbySimilarPlaces(toPlace tPlace:WebPlace) -> Void {
        
        let location = LocationService.shared.certainLocation
        
        if tPlace.source == .google {
            
            PlacesWebAPI.getPlacesNearby(location, type: tPlace.type, keyword: nil) { (loadedPlaces) in
                self.loadPlacesCompletionHandler(tPlace, loadedPlaces)
            }
            
        } else {
            
            AppWebApi.searchPlaces(location: location, category: tPlace.type) { (loadedPlaces) in
                
                if loadedPlaces == nil || loadedPlaces!.count == 0 {
                    
                    PlacesWebAPI.getPlacesNearby(location, type: tPlace.type, keyword: nil) { (gPlaces) in
                        
                        self.loadPlacesCompletionHandler(tPlace, gPlaces)
                    }
                    
                } else {
                    
                    self.loadPlacesCompletionHandler(tPlace, loadedPlaces)
                }
            }
        }
    }
    
    func loadPlacesCompletionHandler(_ targetPlace:WebPlace? = nil, _ loadedPlaces:[WebPlace]?) -> Void {
        
        self.places = loadedPlaces?.map({ (wp) -> PlaceDisp in
            return PlaceDisp(wp)
        }) ?? []
        
        var tDisp:PlaceDisp?
        if let tPlace = targetPlace {
            
            let tDispOpt = self.places.first { (pd) -> Bool in
                
                guard pd.id == tPlace.placeID else {
                    return false
                }
                
                if let pdBranch = pd.data.branchId,
                    let tBranch = tPlace.branchId {
                    return pdBranch == tBranch
                }
                
                return true
            }
            
            if let pd = tDispOpt {
                
                tDisp = pd
            } else {
                
                tDisp = PlaceDisp(tPlace)
                self.places.append( tDisp! )
            }
        }
        
        self.places.sort(by: { (place1, place2) -> Bool in
            return place1.data.distance < place2.data.distance
        })
        
        let targetDisp = tDisp ?? self.places.first!
        self.collectionView.reloadData()
        self.recreateMarkers( targetDisp )
        self.mapView.animate(toLocation: targetDisp.data.coords)
        
        if let index = self.places.index(where: { (disp) -> Bool in return disp.id == targetDisp.id }) {
            self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: true)
        }
        
        Analytics.logEvent(AnalyticsEventViewItemList, parameters: [
            AnalyticsParameterItemCategory: targetDisp.data.type
        ])
    }
    
    func recreateMarkers(_ selectedPlace:PlaceDisp) -> Void {
        
        self.mapView.clear()
        
        let selectIcon = UIImage(named: "place_select_shape")!
        
        for disp in self.places {
            
            let marker = GMSMarker()
            marker.position = disp.coords
            marker.title = disp.data.name
            marker.map = mapView
            marker.userData = disp
            marker.zIndex = 0
            
            disp.loadIcon { (icon) in
                
                if disp.id == selectedPlace.id {
                    marker.icon = icon?.drawnCentrally(on: selectIcon)
                    marker.zIndex = 1
                } else {
                    marker.icon = icon
                }
                
                marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            }
        }
    }
    
    @IBAction func openRadiusController(_ sender: Any) {
        if let topVC = UIApplication.topViewController() {
            RadiusViewController.presentOn(topVC, withDelegate: nil)
        }
    }
    
    deinit {
        
        self.places.forEach { (disp) in
            disp.image = nil
        }
        
        self.places.removeAll()
    }
}

extension HomeViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        print("mark tapped")
        if let selDisp = marker.userData as? PlaceDisp {
            
            self.recreateMarkers(selDisp)
            
            if let index = self.places.index(where: { (disp) -> Bool in return disp.id == selDisp.id }) {
                
                let indexPath = IndexPath(item: index, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                self.mapView.animate(toLocation: selDisp.coords)
            }
        }
        
        return true
    }
}


extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, PlaceViewCellDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.places.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "placeHomeCell", for: indexPath) as! PlaceViewCell
        
        cell.delegate = self
        cell.showPlace( self.places[indexPath.row] )
        
        return cell
    }
    
    func placeCell(_ cell: PlaceViewCell, didRequireInfoShowForPlace place: PlaceDisp) {
        
        if let navVC = self.navigationController as? MainNavigationController {
            navVC.showPlaceDetails(place)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let collectionFrame = self.collectionView.bounds
        for cell in self.collectionView.visibleCells {
            
            if collectionFrame.contains(cell.frame), let index = self.collectionView.indexPath(for: cell) {
                print("cell view: \(index)")
                
                let selected = self.places[index.row]
                self.recreateMarkers(selected)
                self.mapView.animate(toLocation: selected.coords)
            }
        }
    }
}

class PlaceDisp {
    
    var data:WebPlace
    var image:UIImage?
    var photos = [PlacePhotoData]()
    var details:GMSPlace?
    var info:PlaceInfo?
    
    
    init(_ place:WebPlace) {
        self.data = place
    }
    
    var coords:CLLocationCoordinate2D {
        return self.data.coords
    }
    
    var id:String {
        return self.data.placeID
    }
    
    var distanceString:String {
        return self.data.distanceString
    }
    
    private var icon:UIImage?
    func loadIcon(completion:@escaping (UIImage?) -> Void) -> Void {
        
        if self.icon != nil {
            
            completion(self.icon)
            return
        }
        
        if let category = PlaceCategories.shared.category(forId: self.data.type),
            let iconUrl = category.iconLocalUrl {
            
            do {
                
                let placeIcon = try UIImage(data: Data(contentsOf: iconUrl))
                self.icon = placeIcon?.af_imageScaled(to: CGSize(width: 18, height: 18) )
                completion(self.icon)
                
            } catch {}
            
        } else if let icnPth = self.data.iconUrl {
            
            Alamofire.request(icnPth).responseImage { (res) in
                // Probably coming from google places api
                self.icon = res.result.value?.af_imageScaled(to: CGSize(width: 18, height: 18) )
                completion(self.icon)
            }
        }
    }
    
    func showLocation() -> Void {
        
        switch AppUserInfo.current.navigationOption {
        case .maps:
            
            LocationService.shared.openDefaultMapForDirections(self.coords)
        case .uber:
            
            if let userLoc = LocationService.shared.currentLocation {
                
                let placeLoc = self.coords
                let builder = RideParametersBuilder()
                builder.pickupLocation = CLLocation(latitude: userLoc.latitude, longitude: userLoc.longitude)
                builder.dropoffLocation = CLLocation(latitude: placeLoc.latitude, longitude: placeLoc.longitude)
                builder.dropoffNickname = self.data.name
                builder.dropoffAddress = self.data.vicinity
                let rideParameters = builder.build()
                
                let deeplink = RequestDeeplink(rideParameters: rideParameters, fallbackType: .mobileWeb)
                deeplink.execute()
                
            } else {
                
                print("Can't tell current user location..")
            }
        }
    }
    
    var imgReq:DataRequest?
    func loadLogo(completion:@escaping (UIImage?) -> Void) -> Void {
        
        if let image = self.image {
            
            completion(image)
            
        }else if let photoRef = self.data.photoRef {
            
            self.imgReq?.cancel()
            
            if self.data.source == .server {
                
                self.imgReq = Alamofire.request(photoRef).responseImage { (res) in
                    
                    self.image = res.result.value
                    completion(res.result.value)
                }
                
            } else {
                
                self.imgReq = PlacesWebAPI.loadPlacePhoto(photoRef: photoRef) { (image) in
                    self.image = image
                    completion(image)
                }
            }
        }
    }
}
