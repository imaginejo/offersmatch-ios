//
//  ConfirmSignUpViewController.swift
//  OffersMatch
//
//  Created by Ali Hajjaj on 3/18/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import SVProgressHUD
import TrueTime
import Alamofire

class ConfirmSignUpViewController: BaseViewController {
    
    @IBAction func cancel(_ sender: Any) {
        
        AppUserInfo.current.logout()
        
        if let loginVC = self.navigationController?.viewControllers.first(where: { (vc) -> Bool in
            return vc is LoginViewController
        }) {
            
            _ = self.navigationController?.popToViewController(loginVC, animated: true)
        }
    }
    
    func skipProcess() -> Void {
        
        if let navVC = self.navigationController as? MainNavigationController {
            navVC.showIntroVC(animated: true)
        } else {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBOutlet var codeDigitFields: [UITextField]!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isSidMenuSwipeEnabled = false
        
        for i in 0 ..< codeDigitFields.count {
            
            let textField = codeDigitFields[i]
            textField.delegate = self
            textField.keyboardType = .numbersAndPunctuation
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.spellCheckingType = .no
            textField.returnKeyType = (i + 1) == codeDigitFields.count ? .done : .next
            textField.addTarget(self, action: #selector( textFieldEdited(_:) ), for: .editingChanged )
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkUserVerification), name: NotificationNames.AppDidBecomeActive, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.checkUserVerification()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.net?.stopListening()
        self.checkReq?.cancel()
        
        self.net = nil
        self.checkReq = nil
    }
    
    private var checkReq:DataRequest?
    private var net:NetworkReachabilityManager?
    func checkUserVerification() -> Void {
        
        self.net?.stopListening()
        self.net = nil
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let userId:String = AppUserInfo.current.userId!
        
        self.checkReq = AppWebApi.checkVerification(forMember: userId, success: { (confirmed) in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if confirmed {
                self.successfulConfirmation()
            }
            
        }) { (err, isConnectionProblem) in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if isConnectionProblem {
                
                self.net = NetworkReachabilityManager()
                self.net?.listener = { status in
                    
                    switch status {
                    case .reachable:
                        self.checkUserVerification()
                    default:
                        break
                    }
                }
                
                self.net?.startListening()
            }
        }
    }
    
    func contructConfirmationCode() -> String? {
        
        var code = ""
        for digitField in self.codeDigitFields {
            code += digitField.text ?? ""
        }
        
        return code.count == self.codeDigitFields.count ? code : nil
    }
    
    @IBAction func viewWasTapped(_ sender: Any) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func resendConfirmationCode(_ sender: Any) {
        
        if let userId = AppUserInfo.current.userId {
            
            AppWebApi.sendConfirmationCode(forUser: userId) {
                SVProgressHUD.setMaximumDismissTimeInterval(1)
                SVProgressHUD.showSuccess(withStatus: "confirm-sent-success".local)
            }
        }
    }
    
    @IBAction func submitConfirmCode(_ sender: Any) {
        
        if let code = contructConfirmationCode(),
            let userId = AppUserInfo.current.userId {
            
            AppWebApi.confirmSignup(forUser: userId, withCode: code, onSuccess: {
                
                self.successfulConfirmation()
            })
            
        } else {
            
            showErrorMessage("Please fill in fields for confirmation code")
        }
    }
    
    func successfulConfirmation() -> Void {
        
        AppUserInfo.current.isConfirmed = true
        VisitsLog.shared.recreate()
        
        if let nav = self.navigationController as? MainNavigationController {
            
            //nav.showDayProgress()
            nav.showIntroVC(animated: true)
            
        } else {
        
            /* if let today = TrueTimeClient.sharedInstance.referenceTime?.now() {
                
                VisitsLog.shared.markVisited(today)
                
                if let thirtyDaysVC = self.storyboard?.instantiateViewController(withIdentifier: "thirtyDaysVC") as? ThirtyDaysProgressViewController {
                    self.navigationController?.pushViewController(thirtyDaysVC, animated: true)
                }
            } else { */
                
                self.skipProcess()
            //}
        }
    }
}

extension ConfirmSignUpViewController:UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.focusToNextField(textField, withNextValue: nil)
        return true
    }
    
    func focusToNextField(_ textField:UITextField, withNextValue value: String?) -> Void {
        
        if let index = codeDigitFields.index(of: textField) {
            
            if index < (codeDigitFields.count - 1) {
                
                let nextField = codeDigitFields[index + 1]
                nextField.text = value ?? nextField.text
                nextField.becomeFirstResponder()
                
            } else {
                
                textField.resignFirstResponder()
            }
        }
    }
    
    func focusToPrevField(_ textField:UITextField) -> Void {
        
        if let index = codeDigitFields.index(of: textField) {
            
            if index > 0 {
                
                let prevField = codeDigitFields[index - 1]
                prevField.becomeFirstResponder()
                
            } else {
                
                textField.resignFirstResponder()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let char = string.cString(using: .utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (string.isEmpty && isBackSpace == -92) {
            
            textField.text = nil
            self.focusToPrevField(textField)
            return false
            
        } else if string.count > 1, let ci = self.codeDigitFields.index(of: textField) {
            
            for i in 0 ..< string.count {
                
                if i < self.codeDigitFields.count - ci {
                    
                    let ind = string.index(string.startIndex, offsetBy: i)
                    self.codeDigitFields[ci + i].text = String(string[ind])
                }
            }
            
            return false
        }
        
        return true
    }
    
    func textFieldEdited(_ sender: Any) {
        
        let textField = sender as! UITextField
        
        if let str = textField.text, str.count == 2 {
            
            textField.text = str.substring(to: str.index(str.startIndex, offsetBy: 1))
            
            let nextChar = str.substring(from: str.index(str.startIndex, offsetBy: 1))
            self.focusToNextField(textField, withNextValue: nextChar)
        }
    }
}

