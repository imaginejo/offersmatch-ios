//
//  AdsViewController.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/22/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class AdsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var categories = [AdCategory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        }
        
        self.fetchAdsCategories()
    }
    
    func fetchAdsCategories() -> Void {
        
        let memberId = AppUserInfo.current.userId
        let fcmToken = UserDefaults.standard.string(forKey: "fcmToken")
        
        SVProgressHUD.show()
        AppWebApi.fetchAdsCategories(forMember: memberId, andUserFCMToken: fcmToken, onSuccess: { (loadedCats) in
            
            SVProgressHUD.dismiss()
            self.categories = loadedCats ?? self.categories
            self.tableView.reloadData()
            
        }) { (error) in
            
            SVProgressHUD.dismiss()
            print(error)
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    var selectedCategory:AdCategory!
    
    deinit {
        print("delete categories !!")
    }
}

extension AdsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "adCell", for: indexPath) as! AdCategoryViewCell
        let category = self.categories[indexPath.row]
        
        cell.showCategory(category)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedCategory = self.categories[indexPath.row]
        self.performSegue(withIdentifier: "ads_categories_to_details", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        (segue.destination as! AdsDetailsViewController).currentCategory = self.selectedCategory
    }
}

class AdCategory {
    
    var id:String
    var name:String
    var imageUrl:String
    var image:UIImage?
    
    init(id:String, name:String, image:String) {
        
        self.id = id
        self.name = name
        self.imageUrl = image
    }
}
