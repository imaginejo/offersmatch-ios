//
//  PlaceDetailsViewController.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/11/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import GooglePlaces
import Firebase

class PlaceDetailsViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var offersContainer: UIView!
    @IBOutlet weak var offersStack: UIStackView!
    
    var currentPlace:PlaceDisp!

    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var aboutHeightConst: NSLayoutConstraint!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var logoFooter: UIView!
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var photosContainer: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var buttons: [UIButton]!
    
    @IBOutlet weak var photoRightBtn: UIButton!
    @IBOutlet weak var photoLeftBtn: UIButton!
    
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBAction func showDistance(_ sender: Any) {}
    
    private var isAboutExpanded:Bool = false
    @IBAction func expandAbout(_ sender: Any) {
        
        let button = sender as! UIButton
        
        var targetHeight:CGFloat = 100
        var rotate:CGFloat = 0
        
        if isAboutExpanded == false {
            let size = self.aboutLabel.sizeThatFits(
                CGSize(width: self.aboutLabel.frame.width,
                       height: CGFloat.infinity))
            targetHeight = size.height
            rotate = CGFloat.pi
        }
        
        isAboutExpanded = !isAboutExpanded
        self.aboutHeightConst.constant = targetHeight
        
        UIView.animate(withDuration: 0.2) {
            button.transform = CGAffineTransform(rotationAngle: rotate)
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func launchCalling(_ sender: Any) {
        
        if let number = self.currentPlace.info?.phoneNumber,
            let url = URL(string: "telprompt://\(number.replacingOccurrences(of: " ", with: ""))"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    let greenColor = UIColor(valueRed: 0, green: 178, blue: 92, alpha: 1)
    
    @IBOutlet weak var favoriteButton: UIButton!
    @IBAction func makeFavorite(_ sender: Any) {
        
        let isFavorite = UserAppData.isPlaceFavoritesIncluded(self.currentPlace.id)
        
        if isFavorite {
            UserAppData.removePlaceFromFavorite(self.currentPlace.data)
        } else {
            UserAppData.addPlaceToFavorite(self.currentPlace.data)
        }
        
        self.favoriteButton.tintColor = isFavorite ? UIColor.gray : greenColor
        self.favoriteButton.shake()
    }
    
    @IBAction func showLocation(_ sender: Any) {
        
        self.currentPlace.showLocation()
    }
    
    @IBOutlet weak var headerView: UIView!
    
    var footerMask = CAShapeLayer()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let placeInfo = self.currentPlace.info!
        let snappingLayout = SnappingCollectionViewLayout()
        snappingLayout.itemSize = CGSize(width: self.view.frame.width, height: 345)
        snappingLayout.minimumLineSpacing = 2
        snappingLayout.minimumInteritemSpacing = 10000
        snappingLayout.scrollDirection = .horizontal
        
        self.photosCollectionView.collectionViewLayout = snappingLayout
        self.photosCollectionView.decelerationRate = UIScrollViewDecelerationRateFast
        
        self.photoRightBtn.isHidden = self.currentPlace.photos.count <= 1
        self.photoLeftBtn.isHidden = true
        
        self.titleLabel.text = placeInfo.name
        self.logoView.image = self.currentPlace.image
        
        self.logoFooter.layer.mask = footerMask
        self.buttons.forEach { (btn) in
            btn.applyDarkShadow(opacity: 0.10, offsetY: 0, radius: 4)
        }
        
        self.shadowView.applyDarkShadow(opacity: 0.20, offsetY: 0, radius: 6)
        self.offersContainer.applyDarkShadow(opacity: 0.20, offsetY: 0, radius: 6)
        
        self.nameLabel.text = placeInfo.name
        self.typeLabel.text = placeInfo.category.name
        
        self.aboutLabel.text = placeInfo.about
        
        self.distanceLabel.text = "calculating".local
        self.currentPlace.data.getDirectionDistance { (dist) in
            self.distanceLabel.text = dist
        }
        
        self.hoursLabel.text = placeInfo.isOpened ? "OPEN".local : "CLOSED".local
        
        if placeInfo.offers.count > 0 {
            
            placeInfo.offers.forEach { (offer) in
                self.offersStack.addArrangedSubview( PlaceOfferView.create(offer) )
            }
            
        } else {
            
            self.offersContainer.isHidden = true
        }
        
        let isFavorite = UserAppData.isPlaceFavoritesIncluded(self.currentPlace.id)
        self.favoriteButton.tintColor = isFavorite ? greenColor : UIColor.gray
        
        let pr = self.photosContainer.bounds
        let gr = CGRect(x: 0, y: pr.height - 80, width: pr.width, height: 80)
        let gradView = GradientView(frame: gr)
        gradView.colors = [UIColor(valueRed: 242, green: 246, blue: 250, alpha: 0),
                           UIColor(valueRed: 242, green: 246, blue: 250, alpha: 1)]
        gradView.locations = [0.0, 1.0]
        gradView.start = CGPoint(x: 0, y: 0)
        gradView.end = CGPoint(x: 0, y: 1)
        
        self.photosContainer.insertSubview(gradView, at: 1)
        gradView.autoresizingMask = [.flexibleTopMargin, .flexibleWidth]
    }
    
    var focusOnOffers:Bool = false
    
    var isShown:Bool = false
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
            AnalyticsParameterItemID: self.currentPlace.id,
            AnalyticsParameterItemLocationID: self.currentPlace.id
        ])
        
        if focusOnOffers && isShown == false {
            
            var frame = self.offersContainer.frame
            frame.size.height += 20
            
            self.scrollView.scrollRectToVisible(frame, animated: true)
        }
        
        self.isShown = true
    }
    
    @IBOutlet weak var shadowView: UIView!
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let bounds = CGRect(x: 0, y: 0,
                            width: self.view.frame.width - 30,
                            height: self.logoFooter.frame.height)
        let path = UIBezierPath(rect: bounds)
        let w = self.logoView.frame.width + 10
        let crect = CGRect(x: (bounds.width - w) / 2.0, y: -w / 2.0, width: w, height: w)
        let circle = UIBezierPath(ovalIn: crect )
        path.append( circle )
        path.usesEvenOddFillRule = true
        
        footerMask.fillRule = kCAFillRuleEvenOdd
        footerMask.path = path.cgPath
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    var currentIndex:Int = 0
    @IBAction func didTapLeftButton(_ sender: Any) {
        self.scrollPhotos(by: -1)
    }
    
    @IBAction func didTapRightButton(_ sender: Any) {
        self.scrollPhotos(by: +1)
    }
    
    func scrollPhotos(by dIndex:Int ) -> Void {
        
        let toIndex = self.currentIndex + dIndex
        let total = self.photosCollectionView.numberOfItems(inSection: 0)
        if toIndex >= 0 && toIndex < total {
            
            let toIndexPath = IndexPath(item: toIndex, section: 0)
            self.photosCollectionView.scrollToItem(at: toIndexPath, at: .centeredHorizontally, animated: true)
            self.currentIndex = toIndex
            self.adaptPhotoNavigation()
        }
    }
    
    func adaptPhotoNavigation() -> Void {
        let total = self.photosCollectionView.numberOfItems(inSection: 0)
        self.photoRightBtn.isHidden = self.currentIndex == (total-1)
        self.photoLeftBtn.isHidden = self.currentIndex == 0
    }
}

extension PlaceDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.currentPlace.photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "placePhotoCell", for: indexPath) as! PlacePhotoViewCell
        let photoData = self.currentPlace.photos[indexPath.row]
        
        cell.imageView.image = nil
        cell.activityIndicator.startAnimating()
        
        photoData.load { (img) in
            
            cell.activityIndicator.stopAnimating()
            cell.imageView.image = img
        }
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if let index = self.photosCollectionView.indexPathsForVisibleItems.last {
            self.currentIndex = index.item
            self.adaptPhotoNavigation()
        }
    }
}


