//
//  LangChangeCloseViewController.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 9/27/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class LangChangeCloseViewController: UIViewController {
    
    class func launch(withLanguageCode langCode:String, andRegion langRegion:String) -> Void {
        
        if let topVC = UIApplication.topViewController(),
            let closeVC = topVC.storyboard?.instantiateViewController(withIdentifier: "langChangeCloseVC") as? LangChangeCloseViewController {
            
            closeVC.languageCode = langCode
            closeVC.languageRegion = langRegion
            closeVC.modalTransitionStyle = .crossDissolve
            topVC.present(closeVC, animated: true, completion: nil)
        }
    }

    @IBOutlet weak var timeLabel: UILabel!
    var timer:Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Locale.switchLanguage(self.languageCode, region: self.languageRegion)
        
        displayTime()
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector( timerTick(_:) ), userInfo: nil, repeats: true)
    }
    
    var secondsCount:Int = 5
    
    @objc func timerTick(_ sender:Any ) -> Void {
        
        displayTime()
    }
    
    func displayTime() -> Void {
        
        let secondsTxt = self.languageCode == "ar" ? "seconds" : "ثواني"
        self.timeLabel.text = "\(secondsCount) \(secondsTxt)"
        
        secondsCount -= 1
        if secondsCount < 0 {
            closeApplication()
        }
    }
    
    private var languageCode:String!
    private var languageRegion:String!
    
    func closeApplication() -> Void {
        
        self.timer?.invalidate()
        exit(0)
    }
    
    @IBAction func closeImmediately(_ sender: Any) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.closeApplication()
        }
    }
}
