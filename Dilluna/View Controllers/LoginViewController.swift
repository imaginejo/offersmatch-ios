//
//  ViewController.swift
//  OffersMatch
//
//  Created by Ali Hajjaj on 3/14/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import Validator
import Firebase
import GoogleSignIn
import FacebookLogin
import SVProgressHUD
import Alamofire

enum ValidationError : Error {
    case password
    case email
    case confirm
    case name
}

class LoginViewController: BaseViewController, UITextFieldDelegate, GIDSignInUIDelegate {
    
    func proceed() -> Void {
        
        if let navVC = self.navigationController as? MainNavigationController {
            navVC.showIntroVC(animated: true)
        } else {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func closeView(_ sender: Any) {
        
        self.proceed()
    }
    
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    
    @IBAction func viewDidTap(_ sender: Any) {
        
        self.view.endEditing(true)
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isSidMenuSwipeEnabled = false
        
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillShow, object: nil, queue: nil) { (notif) in
            
            if let frame = notif.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
                self.bottomConst.constant = frame.height
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillHide, object: nil, queue: nil) { _ in
            
            self.bottomConst.constant = 0
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.passwordField.text = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func doSocialLogin(withInfo socialInfo:SocialLoginInfo) -> Void {
        
        print("### ( \(socialInfo.socialNetwork) ) ###")
        //print(socialInfo.name)
        print(socialInfo.email)
        //print(socialInfo.imageUrl)
        //print(socialInfo.socialId)
        print("############")
        
        AppWebApi.socialLogin(email: socialInfo.email,
                              socialId: socialInfo.socialId,
                              socialNetwork: socialInfo.socialNetwork.rawValue,
                              success: { (userId, name, photoUrl, date, isConfirmed) in
                                
                AppUserInfo.current.userId = userId
                AppUserInfo.current.name = name
                AppUserInfo.current.email = socialInfo.email
                AppUserInfo.current.photo = photoUrl
                AppUserInfo.current.isConfirmed = true
                AppUserInfo.current.registrationDate = date
                AppUserInfo.current.network = socialInfo.socialNetwork
                VisitsLog.shared.validate()
                
                AppWebApi.updateToken(forMember: userId)
                
                let signUpMethod = socialInfo.socialNetwork == .Google ? "Google" : "Facebook"
                Analytics.logEvent(AnalyticsEventLogin, parameters: [
                    AnalyticsParameterSignUpMethod: signUpMethod
                ])
                                
                self.proceed()
                                
        }, notRegistered: {
            
            let continueSignUp:(UIImage?) -> Void = { (image) in
                
                if let socialSignUpVC = self.storyboard?.instantiateViewController(withIdentifier: "socialSignUpVC") as? SocialSignUpViewController {
                    
                    socialSignUpVC.socialInfo = socialInfo
                    socialSignUpVC.selectedImage = image
                    
                    self.navigationController?.pushViewController(socialSignUpVC, animated: true)
                }
            }
            
            if let imgUrl = socialInfo.imageUrl {
                
                Alamofire.request(imgUrl).responseImage(completionHandler: { (response) in
                    continueSignUp(response.result.value)
                })
                
            } else {
                
                continueSignUp(nil)
            }
            
        }, failure:{ (error) in
            
            SocialLogin.logout(socialInfo.socialNetwork)
            showErrorMessage(error)
        })
        
    }
    
    func showHUD(_ message:String) -> Void {
        
        SVProgressHUD.show(withStatus: message )
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.setDefaultStyle(.dark)
    }
    
    @IBAction func loginUsingFacebook(_ sender: AnyObject) {
        
        SocialLogin.loginUsingFacebook(from: self, failure:
            { (error) -> (Void) in
                
                print("Error in Facebook login !!")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.6, execute: {
                    showErrorMessage("facebook-signin-error".local)
                })
                
        }) { (token, userInfo) -> (Void) in
            
            self.doSocialLogin(withInfo: userInfo)
        }
        
    }
    
    @IBAction func loginUsingGoogle(_ sender: AnyObject) {
        
        SocialLogin.loginUsingGoogle(from: self, failure:
            { (error) -> (Void) in
                
                print("Error in Google login !!")
                showErrorMessage("google-signin-error".local)
        }) { (guser, userInfo) -> (Void) in
            
            self.doSocialLogin(withInfo: userInfo)
        }
    }
    
    @IBAction func submitLogin(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let passRule = ValidationRuleLength(min: 6, max: 40, lengthType: .characters, error: ValidationError.password)
        let emailRule = ValidationRulePattern(pattern: EmailValidationPattern.standard, error: ValidationError.email)
        
        if let email = self.emailField.text, email.validate(rule: emailRule).isValid,
            let password = self.passwordField.text, password.validate(rule: passRule).isValid {
            
            AppWebApi.login(email: email, password: password, success: { (userId, name, photoUrl, date, isConfirmed) in
                
                AppUserInfo.current.userId = userId
                AppUserInfo.current.name = name
                AppUserInfo.current.email = email
                AppUserInfo.current.photo = photoUrl
                AppUserInfo.current.isConfirmed = isConfirmed
                AppUserInfo.current.registrationDate = date
                VisitsLog.shared.validate()
                
                AppWebApi.updateToken(forMember: userId)
                
                Analytics.logEvent(AnalyticsEventLogin, parameters: [
                    AnalyticsParameterSignUpMethod: "email"
                ])
                
                if isConfirmed {
                    
                    self.proceed()
                    
                } else {
                    
                    self.performSegue(withIdentifier: "login_to_confirm", sender: nil)
                }
            })
            
        } else {
            
            showErrorMessage("Please enter Email and Password properly")
        }
    }
}

