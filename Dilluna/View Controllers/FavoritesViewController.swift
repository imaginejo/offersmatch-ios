//
//  FavoritesViewController.swift
//  Dillluna
//
//  Created by AhmeDroid on 4/11/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, FavoriteViewCellDelegate {
    
    var favoriteItems:[PlaceDisp]!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            self.favoritesCollectionView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.favoriteItems = UserAppData.favoriteItems().map({ (data) -> PlaceDisp in
            return PlaceDisp(data)
        })
        
        self.favoritesCollectionView.reloadData()
    }
    
    @IBOutlet var favoritesCollectionView: UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.favoriteItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = favoritesCollectionView.dequeueReusableCell(withReuseIdentifier: "favoriteCell", for: indexPath) as! FavoriteViewCell
        cell.delegate = self
        cell.showPlace( self.favoriteItems[indexPath.row] )
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width: CGFloat = (view.frame.size.width - 30 - 7) / 2.0
        return CGSize(width: width, height : 184)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 7
    }
    
    func favoritePlaceCell(_ cell: FavoriteViewCell, didRequireInfoShowForPlace place: PlaceDisp) {
        
        if let navVC = self.navigationController as? MainNavigationController {
            navVC.showPlaceDetails(place)
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
}


