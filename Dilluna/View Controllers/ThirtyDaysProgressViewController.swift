//
//  welcomeBack.swift
//  Dillluna
//
//  Created by AhmeDroid on 4/7/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class ThirtyDaysProgressViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var _coinNotActive = UIImage(named: "Coin_NotActibe" )
    var _coinActive = UIImage(named : "Coin_Active" )
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var continueButton: UIButton!
    
    @IBAction func continueToIntro(_ sender: Any) {
        
        if let navVC = self.navigationController as? MainNavigationController {
            navVC.showIntroVC(animated: true)
        } else {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return VisitsLog.shared.days.count
    }
    
    let visitedColor = UIColor(red: 0.9960784314, green: 0.8196078431 , blue: 0.3254901961, alpha: 1)
    let notVisitedColor = UIColor(red: 0.2941176471 , green: 0.8666666667 , blue: 0.462745098 , alpha: 1)
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WB", for: indexPath ) as! WBCollectionViewCell
        
        let dayKey = VisitsLog.shared.days[indexPath.row]
        let visited = VisitsLog.shared.checkVisited(dayKey)
        
        var number = "\(indexPath.row + 1)"
        number = number.count == 1 ? "0\(number)" : number
        
        cell.myLabel.text = number
        cell.myImage.image = visited ? _coinActive : _coinNotActive
        cell.myLabel.textColor = visited ? visitedColor : notVisitedColor
        cell.dayLabel.textColor = visited ? visitedColor :  notVisitedColor
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.continueButton.layer.cornerRadius = 3
        self.continueButton.applyDarkShadow(opacity: 0.15 , offset: CGSize(width: 0 , height: 0.5 ) , radius: 5.0 )
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        var index = 0
        for i in 0 ..< VisitsLog.shared.days.count {
            
            let day = VisitsLog.shared.days[i]
            if VisitsLog.shared.dictionary[day]! {
                index = i
            }
        }
        
        let indexPath = IndexPath(item: index, section: 0)
        self.collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
    }
}
