//
//  SideMenuViewController.swift
//  IzwetnaApp
//
//  Created by AhmeDroid on 12/18/17.
//  Copyright © 2017 Imagine Technologies. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    @IBOutlet var trackingView: UIView!
    @IBOutlet var userView: UIView!
    @IBOutlet var signupView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    weak var targetNavigation:UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        // To be removed once coins logic is setteled
        self.stackView.removeArrangedSubview(self.trackingView)
        self.trackingView.removeFromSuperview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if AppUserInfo.current.isSignedIn {
            
            self.stackView.removeArrangedSubview(self.signupView)
            self.signupView.removeFromSuperview()
            
            self.stackView.insertArrangedSubview(self.userView, at: 0)
            
            self.profileImageView.image = UIImage(named: "user")
            
            if let photoPath = AppUserInfo.current.photo,
                let photoUrl = URL(string: photoPath) {
                
                self.profileImageView.af_setImage(withURL: photoUrl)
            }
            
            var dateStr = ""
            if let regDateStr = AppUserInfo.current.registrationDate {
                
                let regDF = DateFormatter()
                regDF.locale = Locale(identifier: "en_US")
                regDF.dateFormat = "dd-MM-yyyy HH:mm:ss"
                
                if let regDate = regDF.date(from: regDateStr) {
                    
                    regDF.locale = Locale.current
                    regDF.dateFormat = "d MMM yyyy"
                    
                    dateStr = regDF.string(from: regDate)
                }
            }
            
            self.nameLabel.text = AppUserInfo.current.name
            self.subtitleLabel.text = "\( "user-since".local ) \(  dateStr )"
            
            self.userView.layoutSubviews()
            self.userView.layoutIfNeeded()
            
        } else {
            
            self.stackView.removeArrangedSubview(self.userView)
            self.stackView.insertArrangedSubview(self.signupView, at: 0)
            self.userView.removeFromSuperview()
        }
    }
    
    @IBAction func showSignUp(_ sender: Any) {
        
        self.showRegistrationSection()
    }
    
    @IBAction func showProfile(_ sender: Any) {
        
        AppWebApi.fetchProfileForUser(AppUserInfo.current.userId!) { (loadedProfile) in
            
            if let profile = loadedProfile {
                self.showRegistrationSection(withProfile: profile)
            }
        }
    }
    
    func showRegistrationSection(withProfile profile:UserProfile? = nil) -> Void {
        
        if let registrationNav = self.storyboard?.instantiateViewController(withIdentifier: "registrationNav") as? RegistrationNavigationController{
            
            registrationNav.currentProfile = profile
            self.dismiss(animated: true, completion: {
                self.targetNavigation?.present(registrationNav, animated: true, completion: nil)
            })
        }
    }
    
    @IBAction func showPage(_ sender: Any) {
        let button = sender as! UIButton
        
        self.dismiss(animated: true, completion: {
            
            switch button.tag {
            case 0:
                _ = self.targetNavigation?.popToRootViewController(animated: true)
            case 1:
                if let offersVC = self.storyboard?.instantiateViewController(withIdentifier: "offersVC") {
                    self.targetNavigation?.pushViewController(offersVC, animated: true)
                }
            case 2:
                if let favVC = self.storyboard?.instantiateViewController(withIdentifier: "favoriteVC") {
                    self.targetNavigation?.pushViewController(favVC, animated: true)
                }
            case 3:
                if let adsVC = self.storyboard?.instantiateViewController(withIdentifier: "adsVC") {
                    self.targetNavigation?.pushViewController(adsVC, animated: true)
                }
            case 4:
                
                if AppUserInfo.current.isSignedIn == false {
                    
                    if let registrationNav = self.storyboard?.instantiateViewController(withIdentifier: "registrationNav") as? RegistrationNavigationController{
                        self.targetNavigation?.present(registrationNav, animated: true, completion: nil)
                    }
                    
                } else if VisitsLog.shared.isValid {
                    
                    if let thirtyDaysVC = self.storyboard?.instantiateViewController(withIdentifier: "thirtyDaysVC") {
                        self.targetNavigation?.pushViewController(thirtyDaysVC, animated: true)
                    }
                    
                } else {
                    showErrorMessage( "invalid-tracking".local )
                }
            case 7:
                
                if let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "settingsVC") {
                    self.targetNavigation?.pushViewController(settingsVC, animated: true)
                }
                
            default:
                break
            }
        })
    }
}

