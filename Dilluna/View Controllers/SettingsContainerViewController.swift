//
//  SettingsContainerViewController.swift
//  Dilluna
//
//  Created by Suhayb Ahmad on 8/7/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit

class SettingsContainerViewController: UIViewController {
    
    @IBAction func dismissView(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
