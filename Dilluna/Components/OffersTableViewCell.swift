//
//  OffersTableViewCell.swift
//  Offers
//
//  Created by AhmeDroid on 4/19/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import Alamofire

protocol OffersTableViewCellDelegate:class {
    func offersPlaceCell(_ cell:OffersTableViewCell, didRequireInfoShowForPlace place:PlaceDisp) -> Void
}

class OffersTableViewCell: UITableViewCell {
    
    @IBOutlet var cellView: UIView!
    @IBOutlet var placeLogo: UIImageView!
    
    @IBOutlet var placeName: UILabel!
    @IBOutlet var placeType: UILabel!
    @IBOutlet var distance: UILabel!
    
    @IBOutlet var favoriteButton: UIButton!
    @IBOutlet var infoButton: UIButton!
    
    weak var delegate:OffersTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.alterFavoriteState(false)
        self.cellView.applyDarkShadow(opacity: 0.15, offset: CGSize(width: 0.2, height: 1.0 ) , radius : 2 )
        self.placeLogo.layer.cornerRadius = self.placeLogo.frame.width / 2
    }
    
    @IBAction func showPlaceInfo(_ sender: Any) {
        
        self.delegate?.offersPlaceCell(self, didRequireInfoShowForPlace: self.currentDisp)
    }
    
    @IBAction func addToFavorite(_ sender: UIButton) {
        
        if UserAppData.isPlaceFavoritesIncluded(self.currentDisp.data.placeID) {
            UserAppData.removePlaceFromFavorite(self.currentDisp.data)
            self.alterFavoriteState(false)
        } else {
            UserAppData.addPlaceToFavorite(self.currentDisp.data)
            self.alterFavoriteState(true)
        }
        
        self.shake()
    }
    
    func alterFavoriteState(_ isAdded:Bool) {
        
        self.favoriteButton.tintColor = isAdded
                ? UIColor(valueRed: 238, green: 161, blue: 60, alpha: 1)  //orange
                : UIColor(valueRed: 94, green: 125, blue: 155, alpha: 1) // blue
    }
    
    weak var currentDisp:PlaceDisp!
    func showPlace(_ disp:PlaceDisp) -> Void {
        
        let place = disp.data
        
        self.alterFavoriteState( UserAppData.isPlaceFavoritesIncluded(place.placeID) )
        
        self.currentDisp = disp
        self.placeName.text = place.name
        self.placeType.text = place.category?.name
        
        self.distance.text = "calculating".local
        place.getDirectionDistance { (dist) in
            self.distance.text = dist
        }
        
        self.placeLogo.image = nil
        
        disp.loadLogo { (image) in
            self.placeLogo.image = image
        }
    }
}
