//
//  AdItemViewCell.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/22/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import Alamofire

protocol AdItemViewCellDelegate:class {
    func adItemCellDidUpdateContent(_ cell:AdItemViewCell)
}

class AdItemViewCell: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var ct_containerHeight: NSLayoutConstraint!
    @IBOutlet weak var ct_pictureHeight: NSLayoutConstraint!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var contentText: UITextView!
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    
    weak var delegate:AdItemViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.container.layer.cornerRadius = 6
        self.container.clipsToBounds = true
        self.shadowView.layer.cornerRadius = 6
        self.shadowView.applyDarkShadow(opacity: 0.25, offsetY: 1, radius: 2)
    }
    
    func calculateHeight(_ itemView:AdItemViewModel, picHeight:CGFloat ){
        
        let item = itemView.item
        let cth = item.text != nil ? self.contentText.sizeThatFits(CGSize(width: self.contentText.frame.width, height: CGFloat.infinity) ).height : 0
        
        itemView.height = picHeight + 5
        itemView.height += cth
        itemView.height += 17
        itemView.height += 7
        
        self.ct_pictureHeight.constant = 0
        self.ct_containerHeight.constant = itemView.height - 7
        
        if picHeight == 0 {
            self.pictureView.isHidden = true
        } else {
            self.pictureView.isHidden = false
            self.ct_pictureHeight.constant = picHeight
        }
    }
    
    var imgReq:DataRequest?
    func showItem(_ itemView:AdItemViewModel) -> Void {
        
        let item = itemView.item
        
        self.contentText.text = item.text ?? ""
        self.dateLabel.text = item.date
        self.pictureView.isHidden = false
        self.pictureView.image = nil
        self.activityIndicator.stopAnimating()
        
        var picHeight:CGFloat = 0
        if let img = itemView.image {
            
            self.pictureView.image = img
            
            picHeight = img.size.height / img.size.width * self.pictureView.frame.width
            
        } else if let imgPath = item.imageUrl, let imgURL = URL(string: imgPath) {
            
            picHeight = 100
            
            self.activityIndicator.startAnimating()
            
            self.imgReq?.cancel()
            self.imgReq = Alamofire.request(imgURL).responseImage(completionHandler: { (resp) in
                
                self.activityIndicator.stopAnimating()
                self.imgReq = nil
                
                if let image = resp.result.value {
                    
                    let w = self.pictureView.frame.width
                    let h = image.size.height / image.size.width * self.pictureView.frame.width
                    
                    itemView.image = image.af_imageScaled(to: CGSize(width: w, height: h) )
                    self.delegate?.adItemCellDidUpdateContent(self)
                }
            })
        }
        
        self.calculateHeight(itemView, picHeight: picHeight)
    }
}
