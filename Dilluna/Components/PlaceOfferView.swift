//
//  PlaceOfferView.swift
//  Dilluna
//
//  Created by Suhayb Ahmad on 8/28/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit

class PlaceOfferView: UIView {
    
    class func create(_ offer:PlaceOffer ) -> PlaceOfferView {
        
        let view = UIView.loadFromNibNamed("PlaceOfferView") as! PlaceOfferView
        
        view.placeOffer = offer
        view.contentLabel.text = offer.text
        view.dateLabel.text = "\(offer.from) - \(offer.to)"
        
        return view
    }
    
    weak var placeOffer:PlaceOffer!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBAction func didTapOffer(_ sender: Any) {
        
        showAlertMessage("تفاصيل العرض", message: self.placeOffer.text, okLabel: "ok".local)
    }
}
