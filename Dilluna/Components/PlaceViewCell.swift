//
//  PlaceViewCell.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/9/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import Alamofire

protocol PlaceViewCellDelegate:class {
    
    func placeCell(_ cell:PlaceViewCell, didRequireInfoShowForPlace place:PlaceDisp) -> Void
}

class PlaceViewCell: UICollectionViewCell {
    
    weak var delegate:PlaceViewCellDelegate?
    
    @IBOutlet weak var distanceButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var logoView: UIImageView!
    
    @IBAction func showPlaceInfo(_ sender: Any) {
        
        self.delegate?.placeCell(self, didRequireInfoShowForPlace: self.currentDisp)
    }
    
    @IBAction func showPlaceLocation(_ sender: Any) {
        
        self.currentDisp.showLocation()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.container.layer.cornerRadius = 3
        self.container.applyDarkShadow(opacity: 0.25, offsetY: 1, radius: 3)
    }
    
    weak var currentDisp:PlaceDisp!
    
    func showPlace(_ disp:PlaceDisp) -> Void {
        
        let place = disp.data
        
        self.currentDisp = disp
        self.nameLabel.textToFit = place.name
        
        self.distanceButton.setTitle("calculating".local, for: .normal)
        place.getDirectionDistance { (dist) in
            self.distanceButton.setTitle(dist, for: .normal)
        }
        
        self.logoView.image = nil
        
        disp.loadLogo { (image) in
            self.logoView.image = image
        }
    }
}
