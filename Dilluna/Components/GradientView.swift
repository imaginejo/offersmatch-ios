//
//  GradientView.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 4/5/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class GradientView: UIView {

    var colors:[UIColor] = [UIColor.black, UIColor.white]
    var locations:[CGFloat] = [0.0, 1.0]
    
    var start = CGPoint(x: 0, y: 0)
    var end = CGPoint(x: 1, y: 0)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isOpaque = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.isOpaque = false
    }
    
    override func draw(_ rect: CGRect) {
        
        let context = UIGraphicsGetCurrentContext()!
        let colors = self.colors.map { (color) -> CGColor in
            return color.cgColor
        }
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let colorLocations: [CGFloat] = self.locations
        
        let gradient = CGGradient(colorsSpace: colorSpace,
                                  colors: colors as CFArray,
                                  locations: colorLocations)!
        
        let startPoint = CGPoint(x: self.start.x * bounds.width, y: self.start.y * bounds.height)
        let endPoint = CGPoint(x: self.end.x * bounds.width, y: self.end.y * bounds.height)
        
        context.clear(rect)
        context.drawLinearGradient(gradient,
                                   start: startPoint,
                                   end: endPoint,
                                   options: [])
    }
}
