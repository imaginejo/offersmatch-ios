//
//  PlacePhotoViewCell.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/11/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit

class PlacePhotoViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
}
