//
//  MenuItemCell.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/8/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit

class MenuItemCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
