//
//  FVCollectionViewCell.swift
//  Dillluna
//
//  Created by AhmeDroid on 4/11/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import Alamofire

protocol FavoriteViewCellDelegate:class {
    
    func favoritePlaceCell(_ cell:FavoriteViewCell, didRequireInfoShowForPlace place:PlaceDisp) -> Void
}

class FavoriteViewCell: UICollectionViewCell {
    
    @IBOutlet var distanceButton: UIButton!
    @IBOutlet var logoView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 2
        self.applyDarkShadow(opacity: 0.222, offset: CGSize(width: 0.2, height: 1.0 ) , radius : 2 )
    }
    
    @IBAction func showPlaceLocation(_ sender: Any) {
        
        self.currentDisp.showLocation()
    }
    
    @IBAction func showPlaceInfo(_ sender: Any) {
        
        self.delegate?.favoritePlaceCell(self, didRequireInfoShowForPlace: self.currentDisp)
    }
    
    weak var delegate:FavoriteViewCellDelegate?
    weak var currentDisp:PlaceDisp!
    
    func showPlace(_ disp:PlaceDisp) -> Void {
        
        let place = disp.data
        
        self.currentDisp = disp
        self.nameLabel.text = place.name
        self.subtitleLabel.text = place.category?.name
        
        self.distanceButton.setTitle("calculating".local, for: .normal)
        place.getDirectionDistance { (dist) in
            self.distanceButton.setTitle(dist, for: .normal)
        }
        
        self.logoView.image = nil
        
        disp.loadLogo { (image) in
            self.logoView.image = image
        }
    }
}
