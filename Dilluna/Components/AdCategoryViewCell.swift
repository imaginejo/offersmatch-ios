//
//  AdCategoryViewCell.swift
//  Dilluna
//
//  Created by Ali Hajjaj on 4/22/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import Alamofire

class AdCategoryViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photoView: UIImageView!
    
    var imgReq:DataRequest?
    
    func showCategory(_ category:AdCategory) -> Void {
        
        self.nameLabel.text = category.name
        self.photoView.image = nil
        
        if let img = category.image {
            
            self.photoView.image = img
            
        } else if let imgUrl = URL(string: category.imageUrl) {
            
            self.imgReq?.cancel()
            self.imgReq = Alamofire.request(imgUrl).responseImage(
                completionHandler: { (resp) in
                    
                    let photo = resp.result.value?
                        .af_imageAspectScaled(toFill: CGSize(width: 60, height: 60))
                        .af_imageRounded(withCornerRadius: 30)
                    
                    category.image = photo
                    self.photoView?.image = photo
            })
        }
    }
}
