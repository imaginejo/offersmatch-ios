//
//  CountryCodeView.swift
//  Dilluna
//
//  Created by Suhayb Ahmad on 5/20/18.
//  Copyright © 2018 Ali Hajjaj. All rights reserved.
//

import UIKit
import CoreLocation

class DialCodes {
    
    private static var _allCodes = [String:String]()
    static var codes:[String:String] {
        
        if _allCodes.count == 0 {
            
            if let dialPath = Bundle.main.path(forResource: "DialCodes", ofType: "plist"),
                let codes = NSDictionary(contentsOfFile: dialPath) as? [String:String] {
                _allCodes = codes
            }
        }
        
        return _allCodes
    }
    
    static func codeForCountry(_ country:String) -> String? {
        return self.codes[country]
    }
}

class DialCodeView: UIView {

    @IBOutlet weak var loadingIndicator:UIActivityIndicatorView!
    @IBOutlet weak var codeLabel:UILabel!
    
    @IBAction func launchCountrySelection(_ sender: Any) {
        
        
    }
    
    var countryCode:String = "JO"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadingIndicator.stopAnimating()
        self.countryCode = "JO"
        self.value = "+962"
    }
    
    private var clGeocoder = CLGeocoder()
    
    private(set) var value:String {
        set {
            self.codeLabel.text = newValue
        }
        get {
            return self.codeLabel.text!
        }
    }
    
    func setLocation( _ location:CLLocationCoordinate2D, completion:((String) -> Void)? = nil ) -> Void {
        
        self.codeLabel.isHidden = true
        self.loadingIndicator.startAnimating()
        self.clGeocoder.reverseGeocodeLocation(CLLocation(latitude: location.latitude, longitude: location.longitude))
        { (placemarks, error) in
            
            if let place = placemarks?.first, let country = place.isoCountryCode, let code = DialCodes.codeForCountry(country) {
                self.countryCode = country
                self.value = code
            }
            
            self.codeLabel.isHidden = false
            self.loadingIndicator.stopAnimating()
            completion?( self.value  )
        }
    }
}
