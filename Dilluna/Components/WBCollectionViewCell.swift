//
//  WBCollectionViewCell.swift
//  Dillluna
//
//  Created by AhmeDroid on 4/7/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class WBCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var myImage: UIImageView!
    @IBOutlet var myLabel: UILabel!
    @IBOutlet var dayLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.applyDarkShadow(opacity: 0.2000, offset: CGSize(width: 0 , height: 0.5 ) , radius: 5.0 )
    }
}
